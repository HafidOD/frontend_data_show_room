import React, { Component } from "react";
import api from "../../utils/api";
//import ButtonSidebar from "../components/buttonSidebarCollapse";

class Activity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      schedule: "",
      accessLimit: "",
      date: "",
      category: "",
      description: "",
      urlConference: "",
      activities: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.syncPartners = this.syncPartners.bind(this);
    this.deletePartner = this.deletePartner.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.handleEditChange = this.handleEditChange.bind(this);
  }
  async syncPartners() {
    const { results } = await api.activities.get();
    this.setState({ activities: results });
  }
  async componentDidMount() {
    await this.syncPartners();
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleEditChange(id, field, value) {
    let activities = this.state.activities;

    const activityId = this.state.activities.findIndex(
      (activity) => activity.id == id
    );
    switch (field) {
      case "date":
        activities[activityId].date = value;
        break;
      case "name":
        activities[activityId].name = value;
        break;
      case "schedule":
        activities[activityId].schedule = value;
        break;
      case "accessLimit":
        activities[activityId].accessLimit = value;
        break;
      case "category":
        activities[activityId].category = value;
        break;
      case "description":
        activities[activityId].description = value;
        break;
      case "urlConference":
        activities[activityId].urlConference = value;
        break;
      default:
        break;
    }
    this.setState({ activities: activities });
  }
  async submitForm() {
    const body = {
      date: this.state.date,
      name: this.state.name,
      schedule: this.state.schedule,
      accessLimit: this.state.accessLimit,
      category: this.state.category,
      description: this.state.description,
      urlConference: this.state.urlConference,
    };
    await api.activities.post(body);
    this.setState({
      name: "",
      date: "",
      schedule: "",
      accessLimit: "",
      category: "",
      description: "",
      urlConference: "",
    });
    await this.syncPartners();
  }

  async deletePartner(id) {
    await api.activities.remove(id);
    await this.syncPartners();
  }

  toggleEditMode(id) {
    const activitiesEditList = this.state.activities.map((activity) => {
      if (activity.id == id) {
        activity.editMode = activity.editMode ? false : true;
        if (activity.editMode == false) {
          api.activities.put(id, activity);
          alert("Updated (=");
          this.syncPartners();
        }
      }
      return activity;
    });
    this.setState({ activities: activitiesEditList });
  }

  render() {
    return (
      <div className="page-content p-5" id="content">
        {/*<ButtonSidebar />*/}
        <table className="table table-bordered">
          <thead className="thead-dark">
            <tr style={{ textAlign: "center" }}>
              <th scope="col">id</th>
              <th scope="col">Fecha</th>
              <th scope="col">Nombre</th>
              <th scope="col">Horario</th>
              <th scope="col">Limitre de Acceso</th>
              <th scope="col">Categoria</th>
              <th scope="col">Descripción</th>
              <th scope="col">Url-Conferencia</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            {this.state.activities.map((activity, index) => (
              <tr key={activity.id} style={{ textAlign: "center" }}>
                <th scope="row">{index + 1}</th>
                <td>
                  {activity.editMode ? (
                    <input
                      type="text"
                      defaultValue={activity.date}
                      onChange={(e) =>
                        this.handleEditChange(
                          activity.id,
                          "date",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    activity.date
                  )}
                </td>
                <td>
                  {activity.editMode ? (
                    <input
                      type="text"
                      defaultValue={activity.name}
                      onChange={(e) =>
                        this.handleEditChange(
                          activity.id,
                          "name",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    activity.name
                  )}
                </td>
                <td>
                  {activity.editMode ? (
                    <input
                      type="text"
                      defaultValue={activity.schedule}
                      onChange={(e) =>
                        this.handleEditChange(
                          activity.id,
                          "schedule",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    activity.schedule
                  )}
                </td>
                <td>
                  {activity.editMode ? (
                    <input
                      type="text"
                      defaultValue={activity.accessLimit}
                      onChange={(e) =>
                        this.handleEditChange(
                          activity.id,
                          "accessLimit",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    activity.accessLimit
                  )}
                </td>
                <td>
                  {activity.editMode ? (
                    <input
                      type="text"
                      defaultValue={activity.category}
                      onChange={(e) =>
                        this.handleEditChange(
                          activity.id,
                          "category",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    activity.category
                  )}
                </td>
                <td>
                  {activity.editMode ? (
                    <input
                      type="text"
                      defaultValue={activity.description}
                      onChange={(e) =>
                        this.handleEditChange(
                          activity.id,
                          "description",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    activity.description
                  )}
                </td>
                <td>
                  {activity.editMode ? (
                    <input
                      type="text"
                      defaultValue={activity.urlConference}
                      onChange={(e) =>
                        this.handleEditChange(
                          activity.id,
                          "urlConference",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    activity.urlConference
                  )}
                </td>

                {activity.editMode ? (
                  <td>
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(activity.id)}
                      className="btn btn-info"
                    >
                      Guardar
                    </button>
                  </td>
                ) : (
                  <td>
                    <button
                      type="button"
                      onClick={() => this.deletePartner(activity.id)}
                      className="btn btn-danger"
                    >
                      eliminar
                    </button>{" "}
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(activity.id)}
                      className="btn btn-primary"
                    >
                      editar
                    </button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
        <form>
          <div className="">
            <label >Fecha</label>
            <input
            style={{maxWidth:"200px"}}
              name="date"
              type="date"
              className="form-control"
              value={this.state.date}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Nombre</label>
            <input
              name="name"
              className="form-control"
              value={this.state.name}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Horario</label>
            <input
              name="schedule"
              className="form-control"
              value={this.state.schedule}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Limite de Acceso</label>
            <input
              name="accessLimit"
              className="form-control"
              value={this.state.accessLimit}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
          <label>
              Elegi la Categoria:
          <select  name="category" className="form-control"  value={this.state.category} onChange={this.handleChange}>
            <option value="tallerer">Talleres</option>
            <option value="conferencia">Conferencias</option>
            <option value="actividad">Actividades</option>
            
          </select>
        </label>
          </div>
          
          <div className="">
            <label>Descripción</label>
            <input
              name="description"
              className="form-control"
              value={this.state.description}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Url-Conferencia</label>
            <input
              name="urlConference"
              className="form-control"
              value={this.state.urlConference}
              onChange={this.handleChange}
            />
          </div>
        </form>
        <button
          type="button"
          onClick={() => this.submitForm()}
          className="btn btn-success text-uppercase mt-4"
        >
          Agregar
        </button>
      </div>
    );
  }
}

export default Activity;
