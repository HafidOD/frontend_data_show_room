import React, { Component } from "react";
import api from "../../utils/api";
//import ButtonSidebar from "../components/buttonSidebarCollapse";

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      description: "",
      image: "",
      ponents: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.syncPartners = this.syncPartners.bind(this);
    this.deletePartner = this.deletePartner.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.handleEditChange = this.handleEditChange.bind(this);
  }
  async syncPartners() {
    const { results } = await api.ponents.get();
    this.setState({ ponents: results });
  }
  async componentDidMount() {
    await this.syncPartners();
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleEditChange(id, field, value) {
    let ponents = this.state.ponents;

    const ponentId = this.state.ponents.findIndex((ponent) => ponent.id == id);
    switch (field) {
      case "firstName":
        ponents[ponentId].firstName = value;Video
        break;
      case "description":
        ponents[ponentId].description = value;
        break;
      case "image":
        ponents[ponentId].image = value;
        break;
      default:
        break;
    }
    this.setState({ ponents: ponents });
  }
  async submitForm() {
    const body = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      description: this.state.description,
      image: this.state.image,
    };
    await api.ponents.post(body);
    this.setState({
      lastName: "",
      firstName: "",
      description: "",
      image: "",
    });
    await this.syncPartners();
  }

  async deletePartner(id) {
    await api.ponents.remove(id);
    await this.syncPartners();
  }

  toggleEditMode(id) {
    const ponentsEditList = this.state.ponents.map((ponent) => {
      if (ponent.id == id) {
        ponent.editMode = ponent.editMode ? false : true;
        if (ponent.editMode == false) {
          api.ponents.put(id, ponent);
          alert("Updated (=");
          this.syncPartners();
        }
      }
      return ponent;
    });
    this.setState({ ponents: ponentsEditList });
  }

  render() {
    return (
      <div className="page-content p-5" id="content">
        {/*<ButtonSidebar />*/}
        <table className="table table-bordered">
          <thead className="thead-dark">
            <tr style={{ textAlign: "center" }}>
              <th scope="col">id</th>
              <th scope="col">Nombre(s)</th>
              <th scope="col">Apellido</th>
              <th scope="col">Descripción</th>
              <th scope="col">Imagen</th>
              <th scope="col">options</th>
            </tr>
          </thead>
          <tbody>
            {this.state.ponents.map((ponent, index) => (
              <tr key={ponent.id} style={{ textAlign: "center" }}>
                <th scope="row">{index + 1}</th>
                <td>
                  {ponent.editMode ? (
                    <input
                      type="text"
                      defaultValue={ponent.firstName}
                      onChange={(e) =>
                        this.handleEditChange(
                          ponent.id,
                          "firstName",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    ponent.firstName
                  )}
                </td>
                <td>Video
                  {ponent.editMode ? (
                    <input
                      type="text"
                      defaultValue={ponent.lastName}
                      onChange={(e) =>
                        this.handleEditChange(
                          ponent.id,
                          "lastName",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    ponent.lastName
                  )}
                </td>
                <td>
                  {ponent.editMode ? (
                    <input
                      type="text"
                      defaultValue={ponent.description}
                      onChange={(e) =>
                        this.handleEditChange(
                          ponent.id,
                          "description",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    ponent.description
                  )}
                </td>
                <td>
                  {ponent.editMode ? (
                    <input
                      type="text"
                      defaultValue={ponent.image}
                      onChange={(e) =>
                        this.handleEditChange(
                          ponent.id,
                          "image",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    ponent.image
                  )}
                </td>

                {ponent.editMode ? (
                  <td>
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(ponent.id)}
                      className="btn btn-info"
                    >
                      Guardar
                    </button>
                  </td>
                ) : (
                  <td>
                    <button
                      type="button"
                      onClick={() => this.deletePartner(ponent.id)}
                      className="btn btn-danger"
                    >
                      eliminar
                    </button>{" "}
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(ponent.id)}
                      className="btn btn-primary"
                    >
                      editar
                    </button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
        <form>
          <div className="">
            <label>Nombre(s)</label>
            <input
              name="firstName"
              className="form-control"
              value={this.state.firstName}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Apellidos</label>
            <input
              name="lastName"
              className="form-control"
              value={this.state.lastName}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Descripción</label>
            <input
              name="description"
              className="form-control"
              value={this.state.description}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Url Imagen</label>
            <input
              name="image"
              className="form-control"
              value={this.state.image}
              onChange={this.handleChange}
            />
          </div>
        </form>
        <button
          type="button"
          onClick={() => this.submitForm()}
          className="btn btn-success text-uppercase mt-4"
        >
          Agregar
        </button>
      </div>
    );
  }
}

export default Table;
