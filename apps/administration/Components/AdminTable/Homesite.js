import React, { Component } from "react";
import api from "../../utils/api";
//import ButtonSidebar from "../components/buttonSidebarCollapse";

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Logo: "",
      carrouselTitle: "",
      congressDate: "",
      video: "",
      
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    
  }

  
  async componentDidMount() {
   const {results } = await api.homesite.get()
    this.setState({ 
    Logo: results.Logo,
    carrouselTitle: results.carrouselTitle,
    congressDate: results.congressDate.slice(0,10),
    video: results.video,})
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  async submitForm() {
    const body = {
      Logo: this.state.Logo,
      carrouselTitle: this.state.carrouselTitle,
      congressDate: this.state.congressDate,
      video: this.state.video,
    };
    await api.homesite.post(body);
    alert("Actualizado")
    this.setState({
      Logo: "",
      carrouselTitle: "",
      congressDate: "",
      video: "",
    });
    const {results } = await api.homesite.get()
    this.setState({ 
    Logo: results.Logo,
    carrouselTitle: results.carrouselTitle,
    congressDate: results.congressDate.slice(0,10),
    video: results.video,})
  }


  render() {
    return (
      <div className="page-content p-5" id="content">
        {/*<ButtonSidebar />*/}
        <form>
          <div className="">
            <label>Logo</label>
            <input
              name="Logo"
              className="form-control"
              value={this.state.Logo}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Titulo de Pagina</label>
            <input
              name="carrouselTitle"
              className="form-control"
              value={this.state.carrouselTitle}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Fecha</label>
            <input
            style={{maxWidth:"200px"}}
              name="congressDate"
              type="date"
              className="form-control"
              value={this.state.congressDate}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Video</label>
            <input
              name="video"
              className="form-control"
              value={this.state.video}
              onChange={this.handleChange}
            />
          </div>
        </form>
        <button
          type="button"
          onClick={() => this.submitForm()}
          className="btn btn-success text-uppercase mt-4"
        >
          Agregar
        </button>
      </div>
    );
  }
}

export default Table;
