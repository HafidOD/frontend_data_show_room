import React, { Component } from "react";
import api from "../../utils/api";
//import ButtonSidebar from "../components/buttonSidebarCollapse";

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
        src: "",
        altText: "",
        caption: "",
        carrouselImages: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.syncPartners = this.syncPartners.bind(this);
    this.deletePartner = this.deletePartner.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.handleEditChange = this.handleEditChange.bind(this);
  }
  async syncPartners() {
    const { results } = await api.homesite.get();
    this.setState({ carrouselImages: results.carrouselImages });
  }
  async componentDidMount() {
    await this.syncPartners();
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleEditChange(id, field, value) {
    let carrouselImagesCopy = this.state.carrouselImages;

    const carrouselImageId = this.state.carrouselImages.findIndex((carrouselImage) => carrouselImage.id == id);
    switch (field) {
      case "src":
        carrouselImagesCopy[carrouselImageId].src = value;
        break;
      case "altText":
        carrouselImagesCopy[carrouselImageId].altText = value;
        break;
      case "caption":
        carrouselImagesCopy[carrouselImageId].caption = value;
        break;
      default:
        break;
    }
    this.setState({ carrouselImages: carrouselImagesCopy });
  }
  async submitForm() {
      
    const body = {
        id: this.state.carrouselImages.length+1,
        src: this.state.src,
        altText: this.state.altText,
        caption: this.state.caption,
    };
    await api.homesite.post({carrouselImages:[...this.state.carrouselImages,body]});
    this.setState({
        src: "",
        altText: "",
        caption: "",
        homesite:[]
    });
    await this.syncPartners();
  }

  async deletePartner(id) {
    let stateCopy = this.state.carrouselImages;
    stateCopy.splice(id-1,1);
    await api.homesite.post({carrouselImages:stateCopy});
    await this.syncPartners();
  }

  toggleEditMode(id) {
    /**
     * const partnersEditList = this.state.partners.map((partner) => {
      if (partner.id == id) {
        partner.editMode = partner.editMode ? false : true;
        if (partner.editMode == false) {
          api.partners.put(id, partner);
          alert("Updated (=");
          this.syncPartners();
        }
      }
      return partner;
    });
    this.setState({ partners: partnersEditList });
     */
    const homesiteEditList = this.state.carrouselImages.map((homesite) => {
      if (homesite.id == id) {
        homesite.editMode = homesite.editMode ? false : true;
        if (homesite.editMode == false) {
          api.homesite.post({carrouselImages: this.state.carrouselImages});
          this.syncPartners();
          alert("Updated (=");
        }
      }
      // api.homesite.post({carrouselImages: homesite});
      // this.syncPartners();
      return homesite;
    });
    this.setState({ carrouselImages: homesiteEditList });
  }

  render() {
      const {carrouselImages=[]} = this.state;
    return (
      <div className="page-content p-5" id="content">
        {/*<ButtonSidebar />*/}
        <table className="table table-bordered">
          <thead className="thead-dark">
            <tr style={{ textAlign: "center" }}>
              <th scope="col">id</th>
              <th scope="col">Imagen</th>
              <th scope="col">Texto</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            {carrouselImages.map((homesite, index) => (
              <tr key={homesite.id} style={{ textAlign: "center" }}>
                <th scope="row">{index + 1}</th>
                <td>
                  {homesite.editMode ? (
                    <input
                      type="text"
                      defaultValue={homesite.src}
                      onChange={(e) =>
                        this.handleEditChange(
                            homesite.id,
                          "src",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    homesite.src
                  )}
                </td>
                <td>
                  {homesite.editMode ? (
                    <input
                      type="text"
                      defaultValue={homesite.altText}
                      onChange={(e) =>
                        this.handleEditChange(
                            homesite.id,
                          "altText",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    homesite.altText
                  )}
                </td>
                
                

                {homesite.editMode ? (
                  <td>
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(homesite.id)}
                      className="btn btn-info"
                    >
                      Guardar
                    </button>
                  </td>
                ) : (
                  <td>
                    <button
                      type="button"
                      onClick={() => this.deletePartner(homesite.id)}
                      className="btn btn-danger"
                    >
                      eliminar
                    </button>{" "}
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(homesite.id)}
                      className="btn btn-primary"
                    >
                      editar
                    </button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
        <form>
          <div className="">
            <label>Imagen</label>
            <input
              name="src"
              className="form-control"
              value={this.state.src}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Texto</label>
            <input
              name="altText"
              className="form-control"
              value={this.state.altText}
              onChange={this.handleChange}
            />
          </div>
          
        </form>
        <button
          type="button"
          onClick={() => this.submitForm()}
          className="btn btn-success text-uppercase mt-4"
        >
          Agregar
        </button>
      </div>
    );
  }
}

export default Table;
