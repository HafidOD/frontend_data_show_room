import React, { Component } from "react";
import api from "../../utils/api";
//import ButtonSidebar from "../components/buttonSidebarCollapse";

class Tickets extends Component {
  constructor(props) {
    super(props);
    this.state = {
      image: "",
      price: "",
      description: "",
      title: "",
      tickets: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.syncPartners = this.syncPartners.bind(this);
    this.deletePartner = this.deletePartner.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.handleEditChange = this.handleEditChange.bind(this);
  }
  async syncPartners() {
    const { results } = await api.tickets.get();
    this.setState({ tickets: results });
  }
  async componentDidMount() {
    await this.syncPartners();
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleEditChange(id, field, value) {
    let tickets = this.state.tickets;

    const ticketId = this.state.tickets.findIndex(
      (tickets) => tickets.id == id
    );
    switch (field) {
      case "image":
        tickets[ticketId].image = value;
        break;
      case "price":
        tickets[ticketId].price = value;
        break;
      case "description":
        tickets[ticketId].description = value;
        break;
      case "title":
        tickets[ticketId].title = value;
        break;
      default:
        break;
    }
    this.setState({ tickets: tickets });
  }
  async submitForm() {
    const body = {
      image: this.state.image,
      price: this.state.price,
      description: this.state.description,
      title: this.state.title,
    };
    await api.tickets.post(body);
    this.setState({
      image: "",
      price: "",
      description: "",
      title: "",
    });
    await this.syncPartners();
  }

  async deletePartner(id) {
    await api.tickets.remove(id);
    await this.syncPartners();
  }

  toggleEditMode(id) {
    const ticketsEditList = this.state.tickets.map((ticket) => {
      if (ticket.id == id) {
        ticket.editMode = ticket.editMode ? false : true;
        if (ticket.editMode == false) {
          api.tickets.put(id, ticket);
          alert("Updated (=");
          this.syncPartners();
        }
      }
      return ticket;
    });
    this.setState({ tickets: ticketsEditList });
  }

  render() {
    return (
      <div className="page-content p-5" id="content">
        {/*<ButtonSidebar />*/}
        <table className="table table-bordered">
          <thead className="thead-dark">
            <tr style={{ textAlign: "center" }}>
              <th scope="col">id</th>
              <th scope="col">Imagen</th>
              <th scope="col">Precio</th>
              <th scope="col">Descripción</th>
              <th scope="col">Titulo</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            {this.state.tickets.map((ticket, index) => (
              <tr key={ticket.id} style={{ textAlign: "center" }}>
                <th scope="row">{index + 1}</th>
                <td>
                  {ticket.editMode ? (
                    <input
                      type="text"
                      defaultValue={ticket.image}
                      onChange={(e) =>
                        this.handleEditChange(
                          ticket.id,
                          "image",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    ticket.image
                  )}
                </td>
                <td>
                  {ticket.editMode ? (
                    <input
                      type="text"
                      defaultValue={ticket.price}
                      onChange={(e) =>
                        this.handleEditChange(
                          ticket.id,
                          "price",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    ticket.price
                  )}
                </td>
                <td>
                  {ticket.editMode ? (
                    <input
                      type="text"
                      defaultValue={ticket.description}
                      onChange={(e) =>
                        this.handleEditChange(
                          ticket.id,
                          "description",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    ticket.description
                  )}
                </td>
                <td>
                  {ticket.editMode ? (
                    <input
                      type="text"
                      defaultValue={ticket.title}
                      onChange={(e) =>
                        this.handleEditChange(
                          ticket.id,
                          "title",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    ticket.title
                  )}
                </td>

                {ticket.editMode ? (
                  <td>
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(ticket.id)}
                      className="btn btn-info"
                    >
                      Guardar
                    </button>
                  </td>
                ) : (
                  <td>
                    <button
                      type="button"
                      onClick={() => this.deletePartner(ticket.id)}
                      className="btn btn-danger"
                    >
                      eliminar
                    </button>{" "}
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(ticket.id)}
                      className="btn btn-primary"
                    >
                      editar
                    </button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
        <form>
          <div className="">
            <label>Imagen</label>
            <input
              name="image"
              className="form-control"
              value={this.state.image}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Precio</label>
            <input
              name="price"
              className="form-control"
              value={this.state.price}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Descripción</label>
            <input
              name="description"
              className="form-control"
              value={this.state.description}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label> Titulo</label>
            <input
              name="title"
              className="form-control"
              value={this.state.title}
              onChange={this.handleChange}
            />
          </div>
        </form>
        <button
          type="button"
          onClick={() => this.submitForm()}
          className="btn btn-success text-uppercase mt-4"
        >
          Agregar
        </button>
      </div>
    );
  }
}

export default Tickets;
