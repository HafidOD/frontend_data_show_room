import React from "react";
import Sidebar from "./sidebar";
import { useSession } from 'next-auth/client'

const Layout = ({ children }) => {
  const [ session, loading ] = useSession()
  console.log({session})
  if(!session){
    return <a href="/api/auth/signin">Sign in</a>
  }
  return (
    <div>
      <Sidebar />
      {children}
    </div>
  );
};

export default Layout;
