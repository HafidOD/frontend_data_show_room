import axios from "axios";

axios.defaults.baseURL = "http://localhost:7000";

async function getPartner() {
  const { data } = await axios.get("/partners/findAll");
  return data;
}

async function postPartner(body) {
  const { data } = await axios.post("/partners", body);
  return data;
}

async function removePartner(id) {
  const { data } = await axios.delete(`/partners/${id}`);
  return data;
}
async function putPartner(id, body) {
  const { data } = await axios.put(`/partners/${id}`, body);
  return data;
}

async function getPonent() {
  const { data } = await axios.get("/ponents/findAll");
  return data;
}

async function postPonent(body) {
  const { data } = await axios.post("/ponents", body);
  return data;
}

async function removePonent(id) {
  const { data } = await axios.delete(`/ponents/${id}`);
  return data;
}
async function putPonent(id, body) {
  const { data } = await axios.put(`/ponents/${id}`, body);
  return data;
}

async function getActivity() {
  const { data } = await axios.get("/activities/findAll");
  return data;
}

async function postActivity(body) {
  const { data } = await axios.post("/activities", body);
  return data;
}

async function removeActivity(id) {
  const { data } = await axios.delete(`/activities/${id}`);
  return data;
}
async function putActivity(id, body) {
  const { data } = await axios.put(`/activities/${id}`, body);
  return data;
}

async function registerAdmin(body) {
  const { data } = await axios.post("/adminAuth/register", body);
  return data;
}

async function getTicket() {
  const { data } = await axios.get("/tickets/findAll");
  return data;
}

async function postTicket(body) {
  const { data } = await axios.post("/tickets", body);
  return data;
}

async function removeTicket(id) {
  const { data } = await axios.delete(`/tickets/${id}`);
  return data;
}
async function putTicket(id, body) {
  const { data } = await axios.put(`/tickets/${id}`, body);
  return data;
}
async function updatehomesite(body) {
  const { data } = await axios.post("/homesite", body);
  return data;
}

async function  gethomesite() {
  const { data } = await axios.get("/homesite");
  return data;
}
const apiObj = {
  admin: {
    register: registerAdmin,
  },
  partners: {
    get: getPartner,
    post: postPartner,
    put: putPartner,
    remove: removePartner,
  },
  ponents: {
    get: getPonent,
    post: postPonent,
    put: putPonent,
    remove: removePonent,
  },
  activities: {
    get: getActivity,
    post: postActivity,
    put: putActivity,
    remove: removeActivity,
  },
  tickets: {
    get: getTicket,
    post: postTicket,
    put: putTicket,
    remove: removeTicket,
  },
  homesite: {
    get: gethomesite,
    post: updatehomesite,
   
  },
};
export default apiObj;
