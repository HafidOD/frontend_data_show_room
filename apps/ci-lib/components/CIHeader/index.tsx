import React from "react";
import Link from "next/link";

interface IProps {}

interface IState {
  isOpen: boolean;
}

export default class CIHeader extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = { isOpen: false };
  }
  render() {
    const {homeSiteInfo} = this.props;
    return (
      <div className="relative bg-white" style={{ color: "red" }}>
        <div className="max-w-7xl mx-auto px-4 sm:px-6">
          <div className="flex justify-between items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10">
            <div className="flex justify-start  lg:flex-1">
              <a href="#">
              <Link href="/">
               
                <img
                  className="h-10 w-auto sm:h-12"
                  src={homeSiteInfo && homeSiteInfo.Logo ? homeSiteInfo.Logo :"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAa0AAAB1CAMAAADKkk7zAAAAkFBMVEX///8AAAAFBwjn5+e7u7tGR0icnJx8fHxzc3P8/PyXl5f6+vrw8PD39/fq6urz8/O0tLTb29vh4eEoKCjV1dXKysrFxcXR0dG2t7empqatrq4vMDHCwsJrbGyJiopgYWFWV1eNjo47OzuAgYFISUofISI4ODhlZmdbW1sNDQ0YGBgsLS5QUVE/QUEUFBQhIyNqtb65AAAgAElEQVR4nO1dB5uqutNPBOktSDFIFXvZ/f7f7s0kgIqA7tnT/u8989znnlUpIb9MzcyA0D/6R//oH/2jfzRNobz800N4gyyvUgxqiQ96mmWFSl6eFKpGEvmOID/ylMrwe4fINI0lcUCwNCr754/855KhZX96CK9Jyn07tKRawGUoEnHl9Uu4NPxE6uMRuvr4c/xLRv8TSVG1Pz2E11S7wfWwiX0FPoSKdNA0O3o5bjc0NieMZ+w/IHzcGE7/EMlP5pgfgDeer/+S0f9E+l9Ay8xQ4SBSuHy2azvzkKugi/XGqb7aYDXDi3DkmCUcgrO/Hir0F6MVZnmnRvbI8Fw9cynTO47hr526ltGlzyjDVLVo0dFDFMZ/M/n7Q/719LeiFakoytsPWoCkva6kcopQIIeq5BEUvzlua8vhwttxpeSWGJ/f4dQ/Tn8lWr6mFmz2spZ7yJouV7qnxExxxaGRuDmh8/dYC6GFQOsqjR9SYbz77ph/C/2NaPmZ5IAWiav2G8uoGTepEuO2KChSJBne21fbCbRKd/wQprmu3xnwb6O30HKIXF/O3Mg9X2qZvLusW7JIlBc7Zp/h067IU/JC6hBF/Ms01f3XvubWCCXSNfjSzRu0igkrIvx/hFZaXY6PfskmT79wizQvH88/XvJpnV43y0F5MOPsTGdjNZy3ZaCgq0Bryq2MZ3j9pWv+KXqFlqQsnp1MYBKlHxcYJscYPn9RTcy5pZn837B6+HqPmHdbBYe3btxRg9bUYwZ/J1rP0mAaLUv5HJxrjpfx2oxyqmGsgD6rcb2/TMRw9w/fCrSiLwZfNgItdeIQcvztaNnhq2CMZWyevptCS5fHseLznUx7lCZdTZ5/pmOBOVMVSNKHIOYesbFW3tS8D1CDVj5xiP8CLWvCnnyLepqWLBXmNEygZYdJvsCrp+8n0JKyybkGyoQ4NDn1z/f3L88/jIlTR0CiP/ARoOUqtBo8Y5RKgZYxcYj0MWll2MX1PbE/Runp4e62doSA2MQ1CV/mzytoHK34+nKymZsC0rC6zhlde5ZHOs2YghZj5oYnQg/V/arULBWFqaGMP+UQXfCLUAZbG5No6Rp7zu+E5wl70MdBW9dptBCS8ZAuHUVLfmOuMYZ1Hjd/Jw/nG2+dj7ExLE1NlVsh4f0k10RFilN9Ea1GEk45aNNoOcAI3wlMqRBZfvyKvkKL4fkFSfgeWAe25JyWBx/mI38TrKddjJZ8Hma1tQZMPyCW5ni5iiotjp+j5fboszc24ZTPwdCaj/+qs2eZ+Pk1Sav+Umb++Au03MMX0EremuhPuGGnne7Rqt8GC+N6mLsS/oBKM8tJLidC+EuU0qwvmMx6N6a01wKtqU3XabSQfVl9U2/hqveML9Gy9++jFb43z4CP/PCpoXfFoKBhA0Dn28OtEe/fG/NKb6Uikz1bPfLcu7ck4TTzfHc35ckC+5loxae3ZhnOJDdb4jYf74nRGw2ve8JHVjWxc+2Odw69mB+TVTO8zYfndPUaLemILxM//wL6mWit35rjHdgBh9vnbj78cZd4mLbDUiwBe4I0nEdu41zWuv2ATM73E/F+MHC7eI0W87duARKdgJg1H2XtaOBFfxiIZA/wEZB7dzk45QEtvb/MdNiBfRetNy0ECOLdi7xuPt4x/R/pMmwgAz/ZKsPASWhy7p6uvmTavY1Gxf4w3gwGVxq0+sLznhharaA1U5VNY2xoWb7sJtEy2MTJSuIllCoS8/kU6lHwLYiiaUYXzCTKZ4gCreLDkGiuVkkz5rBsxYcu55qqpMoNrZRBkNObpyJR+ByVb6IVvCcHwXgPPgbQol8Gq++OdCOBOUzYdc0ytqxu0dqWW96JEXoUeJyiwYt8vva34jaMGB1W7GJO8wRFM3vFGQxwcoHvzgrDImILf+cjXTllsrHFe1hrRL1s8SmI2IzAuqKLSgoK2DYzo/z60S4Wf7euqPaJTy1afoEvFZN6p9YOodtSUcrZ8Th7D63D0HSutIRYluV72k58AbrDeuCiBi1/MNz0sd5XiexV2fpj6OftsBCnFCwImM/H4GB8Z3OkIgkGH4bFqbl9zVtRY6PonvqBZ8d9QRNu1MI5JuUosb+kLW7DReYV1EDFz/LZrRmCqcYebKVAzg6DPhXMWmCwj7a4teDjUwHIWjlIbnhkc4PPkljggvsqsXAJ8xLfQmvIHrzIN1FlyxxOuNejnd6gVQ2cf8o7caGH1RDvjlh0BZsdA3imaC9APMZjxs17ihYCrGpQX7Dhbl/zltwxtw6erAYPC7JKOHvWRaDFHrd1BJyVwpcJV2eKkDMoY0CfDgXDJkWl2KEhWFy+WSz2SpwBNqxAi0G9YP+67BYXcWgTyiXb9yThM2sdk0e1Yi+3/OF7pp9Ayx2AIntUKI46cMwwc5EMsgnZH0thBpiwQFJr1fFR+MHB+hxLaELOSaA1FQFJbmLc6IaywXgtorl1g5bD/l3wb5ZbG1kl3nA0A/Y1PGAuQJFKNqNb/EFh+UQwc3aLVt1i0VkZpMwSU9z2hLggaFaV/Z6VEc3687h5FjG+wUbhn4fQevaLF8/6JH2WliMhjYoivXZcwxCDALGHt25SKmIBEe774sV4PLtFayqqq9wSP9m0zez2QU7isnmDFmc8OFAvDA7SykuZ4QHLGxZLxR4VjAWLnQ9Tc6ANu7doWbtOhHQ2oSVuxkThkf3jdQMxtbfQqvrTuB5LaCh6B3K07Cfr/zS0Me8/ofXR/fZ4vyJAkZY3WxY6DyMBssnBYNclZ/F5IueilYRTOyaVYA6gG1ps5rAYeosWipl42uvwryVCfZnGqK5rvsFTcdOjvQxf9GJNtWjJjchEPQve9sNM8Fbd7aO8h5bV29THuxF9AAtyAC3vibOGRZz0xF2NCHDVi3YvOOPMNLtLmMKi2Bo6MmWtji7czxpxiwXpny93I4FnntECbdNDC7QELL4SGJXN7OfDje/RsoDhZiKA0KFFbxLkDq24KnflgstYlzFAI4jeQyvuzeFsLBrqbwfRevLVxhRK/0bNg1hrish9lMnd3HumjVgTNlbCneLji7KL88u8DGeDN61mnkYrxgC7vLOabx94+h4tBs2Gjw4m/w6tFoAOrWDOLRyD/8QG0srK99Dqp/qPCRD9OV4BaDn9r8cFUF/Brbi0o7Wl2EoHUILut4olo0m85auWAHR49Sqhp4nBH8YZ0P+8RZan0eKOfzznOhAk4YNGvkPLZivISkt2BEz+HVq4EektWjBfhrjtlX26dMlX76E1783hmEYYiNsCWn19tBsPhkm73rFcYtehXBkp/zOMLLUw7tIXmAE06wivVyBqypfJIfsm+3N8JNHdhsYLtODL2ZZPCgiHTaMmdPj3Di2pWXpCFLZoJbgTyC1adXMOO3fDDcHWMQS0Fk8j7aHV540x1hiK23roeadlSrf3AefDzOXE17gKM4sq01Bws7wjfAfWjLuhuB7TqndPKND6GLXxmTw5dT8CWk7ziDe0Zi1aFnitQsNaLevAWUt+GG49C6uZ8y2fALs5J2DKYyZOpo11yUQfSHooTbogYZysuFVoHu4Nr+5ZHtEKe5GGkUe0e8Z7h1Y/lWMq9a/Ph3wgsrqkkW9Zri5dbIjddgJM2j6Axa2Nd9IaA/yqagHjeedQVp0HDO4xv75esL/aPBpmW7XhyAgGfSCuS0oe4NZuW8wuPnP+W3GBEeBGbWZ8gfmuVDEvEeemDmgdQ6Qzoc44ydQliEhtqeMSvmsY9sOnPbR6Jt18JNlnoJBNoLV5/Opzch57olB4jvsopEaV13nR82er1sAAmr3M6OyodbjOY4oruYukhOCAzJfssQmMbrYk3MdjqIRiKnR82/TvrGJI2NcT2Ds6y3x9WszSl+BMYC2XuzpeYN8idRsRH1AQ/PuhHuY5M8Wvm+g2/yqfyf7mRA+tXkR2pKhpeGcZ0OrZ5WM7hIJ6IY0z/1LaU7ip60g9lSmCFjMjDutFC9c7xYxMA0wyFwSWWp3mby6aur8uCscqr8W+3B1LFK/n+2Kz2DYZqXlxOzXmU7rjoZ5gsSmy4rISZWX55bgryhM8ipmvyqy8nlZMTkl8mX8qVoLn9ZI4Ik1iQS22FvFaZken3F/dRO5hdjAi0mOuabSGZ9sZDtJ73Ll5oKlQ6hMjn5qvZTVP5DR8EKJOtIRIO76Kb5eNoVeg9IaBLnuDGfJys6GyHc6fz9tg+4+QE4Zj1osfhQNK1YpkWGJ+d1Ysi2E5LRsROWU/6sHQJlIPrZ7LO2gk6H278YaW1cNxXLMD9eLHx+4HJ4jD5X3uRXDFj/EjX0ScVtFm3x0GsbYhZrPXDXNdh+zHJcZ4eKvlL6QfQGts/8p75rppSRU8Hjx7/LW4QQ3rY/aYC+PvcKPFlrFQJPFsLQ8Lg6gtj3wGUwfD9Ispb3+QptEaevhoBKyfxVtWuKRGrmqrm97Su+Lh23690UGAMZc40qZGi2FFq7XHbnt5vlyPlJOD/Kvo61ZG36m9R8vshaOmg0LLx4O53jKVTW1QOSTW3a31RTvdIAn9gJtnHYKzblE4I5F2N2uPxafKd8SVTSng/oY2ERL+26iHVm8CB8o/h/amOrS+bRPqVW4FXpIs0wftXWHc1fWkJzwHBVTgDq89BVKM62yEma19dyyYz4aiKFWzCV6P50z7kUdp4oVfLS78ddT3jnsh+CdBP5F8Bmj1DJDz5L17myuQ0ucXpE5iicTpsrxPE6JlcRShdGipIPZC58XnDQJBoxpI91a4fzDQZszAcNL9uRHrs9NKi/4OBnwReeozhz0O1pdjGVJv35OnJypUyg2OU9YzuBetd8z+B+rH1FvdhS/Zfq8xu3/KtnOSy9OI994IYwWVEOnbcxvbub5Rrfbr6VVU9/FxzKkiH0Crby9OVe/0N8iAYYhB833KccoT6wGv+hZ4wo11Hzc7jbFu2+bLfFpbSrLranE6fZy2n6vr3pNGsLJy4Kp1BWlDlhOqKx6S3EG1Wf5nayj7aPXV0iNzTabHD8XgJ/oeOP20Q56gl9l5SPlJcU21EN0UUXiHVrPBETQ7jV/oz6SDLxcTaQLbCOTL+s6xN5cXjtdmqdw2mf8I9dHqZzw9hK7JFFgcLau/vzXOXFXvSL7JYKuVRZuJMlGgGevO2HCLDq42iEQbSVhW3s+zBJQjQ+bweD2Ximxg9svr3m2/kJ52+vvpfnd1Zmb5Eq1+V7I2teGZnpBvan/S+34BsuXfNiOFQ8xDGDCXULrZosfXlRr/hFZNkFQ1G9iIIZtmZbyNFnm9mfNlep1FM+94/0XG9UhexrAs9J/ctpGYYnaz5IPWPwB+13cdVgu12HBz4Cc0agHDBa8HYLfWX5K6jrr7BYb/E1rpEwpt+6unTIohtJyn7JjrkPHrPhdGjKh8716W0mxzLXjTR724GeRCLvqp+iKK/AYpwFmrwQVm8zu+QEvm7huhoJN/B1pDidV8S8AdTInuozVUZzeQT/gcxR/JSZK1hxi3LvkC1XDWKJL1ZzeBevHdzpgBV09jWytQWzSNFtnyBCzYIsDH34LWQBzwwx/SSMNoSQOn5498Yxv9NDg80o/AzoGz5E+vrwNMnps2UxdsahMuqs3rBqn750t8hSx+1efklYb8j7so1yBt22lgS/73oDXAXKD3X9e2NhHXIe12yru9RVcazIMfjPb7e7gzZB9kvV0k7hfj0qJdmo65OprXb6LFUzgmxKmCpytieUoU3xGgvw2tuL/ywecaYJkRtMhgrd3sUNFlKlPj8JS5DfQxxFrRHqAIrrBiP5Q7/nR4R0+eb3L7VsOLyfTp18RzS/HnRCuT/YvtFWY7iSEkvw0tVD3OJGSN2P006nG0vlh0LGjALdMrLkD9PSF8QJtWY9jeQuis/YOnKtWbNzKgJudCpGlP+AH+q0B1QMXZvxGtx02RD/CY3kGg23t6rxD2njbPM2RpwsaEmiAUQgLEaW9wKePzwMJpi0+PKU+6+9Lf6h/woE8d7lJNlg4xMb9/y6n7nWjF99tUvPj3nSnv0HqzyPxGp2fVnYqgrlQIEWlrQoCCnJGYsl/lITO2V1+bEFe+F6fIDpLt/QVSDtZxcts/OL3VfPm3onUffwLFbb1VSfzjNf3PFdxU5ZOily2OOt//5eFcjbmAROf1aF9p0EWyDc7uRaW6fYz6iTuMVFm0wzgcTOTL8n3YJFChtLyt/hGx+hYt6wFbO/hm3Gqku0kXSz8/10CO0Z0I+UpzkwFVYGlCl1tlF7ciTbh9GUCS5bwtG/hKSkWwE1WPHRn4eD+Zon7lRYNdh1Dm47E7F6L8KlgW+IikC++yLIVGk+kOaFkSn4aifYbwct6fn6zbL9FY56DWYAfV8BzeGKCjaMyELLoWzXbepqeCnuDSPGBxq9xvA7jnY2dlQ8HHSO+GQWIeY/EQV6F4e4cWEcvh1ZZIiee+E56ZrpVEVwuM1xCVwqdYNN5s0ToFTULzVsgHGRcScvZ497VGsw802pVLBPzgx+dSqwHaN9PmXURJ5utued2ZPbB02mRK2HdZTwiOFIEm/j8ea55DlOgLOxh5r8YheUCryZd/0dEpErohbssLIGt6veNKNULO7obW7HRObFeGvPmZxE/kAS3INf3xbofjHe8IU1agUvXXbQY/mvCQ5YF5Ikoj3u6h1r+v3Frz2Z0jSrRE6fLgcbE/AJBgFox1iRqiahIttWm6MX2NvHHkt20M+czgUOKowFCtn9+hJdxk54BFzfOuyTGtvhN8nugm6R94OOh1+4us8YSWIgu+KWR5isYP0nPgoOn3qWu9JnPBbXuruR8v+HgzkusQ32p5SyKi6/YjWsVbvGU3ccpLk9kGqY4gPHW+hdpVaN1sQujbwAQwM7wy27Ici+myH+/xNdmplRdIDMT07umYCRnoym0Moy07IoONNx7oMqB2nBJ0pV73Ubjc0Gq5T+Fe8juqS1GpurlytPSkpOpK1FPdo9X0CH2z1fi6yccGtG7xrgG0wIRdS2B3fczn6xVvs/7DluGrntX29A4kzoR7oi9vOSpdkZitDEaZbmQMZhLZWQpZGb1vwdRp0Wq3NBzY6ee9RV5QBccEC46WATUKBsipB7TMpkfo65cuuD7dg8rkIL1GK+bb4kyPX1zI83AZvbzFGL1Ca9IWPx6ETLLle4fsrqTPyp6qkzvaZmPuY7DX835EwS5Onx9YlNjdDHfhIk0HgxC4j9yjUwAti3NPCir5AS27QWtk9/T2SMoal164atTPa7QcjK8OpB99sTP6EL1Aa7JR4b5x+71H/nsowCTVUGEe8+OqCXFQHZ6UkUkCP2bMscFbJaHtDoovLMRXbVRVkW9AAS2KT8W+mG+v/rAkHCx8uJHNzL7a5blhotnQa7S4T8/k4WDRxNdoGi1pIuQ3j8WUyX1Z2SuXtdKiH5X/LOQfGbl9PRP9oXzH5CnTeD2ttu2V2JXiaOVs1lwRUXxE69B4dNNWC5M1c5c37hDa8y3eUoS1+O2d7RdojRvvRaOvnrAaKm42I1pll/XqvFpfsopGP9gA2vpkHulHef+VJxhiOtRqf4g4C0er6urEemjtG7Qmd13AXYINan3ViONxtJryZeZ1H33ubYgG3/DNDztck2gtn5Bo6NpMeDS0kfKMFpBtSUDWN1p1W7i00eYxW/vyuosTSC9eK8zRSps1zgaS4NMdWk126V3W0AD5a6F/rLZCHDpR3MIfedsGA/ytJvqZicFdwJUH6e/SX2MTkpGO7oXYqbCjzeDPw2j9BLJACqqPvbqbCMRiMpxT8YiJdeCRjxPeytBO0AFOuDPPkjYtZ0qsMhcPnxTilcwS31JD4p26brv/EMvgIAKTfXJQ4iaUDAX9+FOr6suLOqkpmkJr2Hi/Nt3v4rENyl+Glg8xd/o4m3rTSnJyI8PhQ72w51mHyOdr7OLwPza3alOrRWsyFUfET/dEYorrVOsyPm4/T3jD7Rwrw+fzCZ/YBfIZtDVUg6A+tWzvN+5n9o3XbEygVQ1BcRD2lxmP2x+DaIUUitrT5BshTSSDYk97WUdt5cKkLHRopnqWR0MCbV+XlQGS3JfDME1vkrkpEnsMzD8PolYTXjlMKeMdkoZxEIcy1+JWGgYkZtdkaFFkRVU5L+hN6rlpVWRK9J0U1XG0goGMtOuyyQ+bCtoOocUTqfb58VuZEzlAEvd2w8h5sgb8C9RVW061ZPmzNIqW9VwDeRERPDN4rq15hRZzNwp+we+8ALo4eYBWD/AmGouz75ZYBaeGuaaAD5Q/Wck1itZTEKPlq2iwtckLtGIvRb5XJT+sYPkOCWyNBjM2YP3OtHRESs3kazzfo648eTz6FKzWf7JScgytfgR9LXN1rAevk59atPwqzxWuA4JaoVx+yUZesRm3U+0y3xwMYS+EKvugTeJoGUWC9DMY19J17jDXorzJQ7Z6Pvae9/2aBb8LRI5tw0i7bybBfZNG0JIe40XrZfNqwFd8dY8W32qJur/Cpq1/CkXbDYHJ0m6E9V/1cUf6Bdq/RWLrVzvG3BHswk0xxuXPWfBql1s/jIl0+XUG71s0gtbDZsfCE1YSyV7snjygJXYxeRCBM2opAlmz8G7H7OzcGTPj3JUwg1tirhXfVK5gKol2p6fk6nsvhenIutzgGlg74Qqf/2j51gha9zuQ8+Z9g8G7m/cNWjaHljdxTBpGEmi5ELBfGZkAE/KjPmqwQMZfJaLhkiegKnCxAJ9tWAD7X6BAyK2UXOtnkurG6Y93rRlEy78lBJ4SMSl+9naSYINWU9R6Q6vQBVoOGAU5dNwEiQMLY+1CScTpeSANMUQDO8XNi44zrEnhGzFSEep6l0QXjfiz011b+T5IJiVMBrQd9pwvXfk71HP9htDSu02pTeNzkjcSq/toVeJT3KGFQ4GWJJgK2pJiTYffVpKxWu8mso2YgVqeVp5gcnYiA7p4GcWv+QsS36WmSi3s+ouyW3RvMolyCAbMWs7KvnTlb9C1l3E+hFar94/CokO++qXk2wYtpvquK6GvG7RKG5prhZzpEtHWeW9yv1m1Jd+fUj5xnSU3yRcmS/LaAnydaPBAbZPVzX1jjWuW11njed5MmXeSwH4O9cAZQKuJvK9bvno/2UyQQAuKJCtN5Jo0aH2EB0CLp2l7osliYZvczHz9NvevFySE8peolXsWdIboUkAE8QrMm1scfe3K36BeMOEZLZPvHR6pWEmkfssOfEYLXpYj5xjvpFsq6fUMaAX3aLnIERHHL74X91eSD+0q7xsuM+O1/rbv/TPoGS0wvM9NfIW8LIgcRQusBwLLVBZolc32So+32H1cg+vJp3dB/kEiyf4uSvqxT/6s4d7RE1oe6CuxkETd1I+hBW9NwdwuVARaWdyi1eitDi1IpRZf/UVkuv7SUDVNrTzf/QW9FH6M+mj5eFs1aYv1Vwt77tGCNifHHFhzb3O05lbZoCUJaPjnzK4OxaHSQZG/mcn3X6YeWtaqavjqK3UHA2jd+noeLY7WtSl+YP4W/KMg2M/DNbfcNryb13SD6/8EvTJ0e2iRJqFd/eqLOvto3aXBBwItXbQfZ2gBz2Z2KECDA3cS8Nl0e7z/AumTdZlo2N/yv1Z+NYgW9GKrFQV0U9WiFQu09DuXIL1r+PBbXmJrMjEv8f++Q7GS2KaPAsP2Kzc0lnLu2zShKDRAxQUK1akC2+WeESPFUPKKogC57CAlRmCwyFRx2GGKCVdin52KeTBJ4FEvMSSa5CGi7MzQ6JchPqNl5990/gAtG/hHEtmjmxYtnUdEwrt6MHh3R+fETr3j9qeQW10spVwutSTRXq3iSQoMRAOyQ2hT60miIEUu2Zcm9hyY+kBGueIZSGeoJQspVOyMiaxdjOgKKbYOLz2X9k5B2CquQ4oMGIqxQv4BlVIUXIMU7S3NQTVztrTenPTQ0oMfsdl7xK4jn1cr6EPmr1arteN9rlYXeMvYjn0MEUpLbrPvREq0BqvjeP3lYCHTrAPqfEZWZjjf6qyRy8g1w4wgepDDA0E6m2kU2PklhthUEiB/zzA0A0pDug9jxI4MgPM0xkgh7PS5V8+oQuTs9zEKIGsnwUTeo0JdIu9goywudeTkfv/lpk+9P/Pc+C5BDMQlPs9x1H1Gpt18QvCJdyEKwjCMuzccwIff4X36KcnRVbFqDX0r94LXv2i1imRptYT8cqdkaKFlwLsMUAZDljDeiilNUb73AS3FuKDQ2m+QUR10ZGV1wsScU7OTAxiKp+1JgTTCxN+coRuvTWSrDK1eiv+rqoX/T6TXNL4EXhHEZfzjOX2M4nmQKgTtLAPJqnchqT2X/QAZqKLs61hDdBmXTmoplOr2hejXgCyRFi4taeWHaE+Y9yJdvdKUvShj4Co+MuKdc9ELP/CSeIOKUDNI5FWo7gXk/ktoSZVhxSnoFX/5vbSA0CByYHsym2cfyYaDHBoholi2xcwIq75mFpN4PooSKiPLdZKULPVwyTiNhJ4pyyiurMAPFLZkQiVACrGVwJfqgFkj6dIxvMDXmRmDIqUfQvkvofU7iM2vpUvGO6+auj/nTXpCy+aN2P3A4T1oeYMHAmLDcZkyCgIXOUzxSDaySCD2U25CJQjFjh4Ejl12lMXDSuzAwEZdx2GSQt86dqAbgapyRFQn/k7m2n+H+miR9XqXIfd8vVJErvNrZtsq+4qg+RIZ5806RZrKWyPsd/O1Aa8xWK8P3ELQ2V9XH1mH9ZrZf8nGQhnsben5bn4NUdvlJ1lfD46LfRSurrtKR7xLmrNn5/w1wbi/mHpo2asESbllzU0fSzEvEMhLGyUJWntIFCxqmKKNjBYhM88TxMwrdgTwVH0wUeKhgn2h7hHFKipgF0TPOO/hptJhniAlYGj5xxBJho14kfGBnVN/txPkf4H6b8YQoVVr5dGVRRbFhaCzSAVjaFWLA+OrC90FpYxWETRjcqFGW4JSDVO0JEle+lgAAAZlSURBVLHgDVn6yjGMzVLjaNXrQ9WhhcLTJkQultJPZr8yh56jBa8CNydrO/4Rpx5a8Y5NPLHck4ZjFGOJ6IiJROQEiHmvueEzmbeRw/kuReDlVrXO/kIB74l78fhx6xBaCpqGYq934Hnpmgf9DppsITNA0YdsYynEyKWbBPFGTqcI3vz3N7zM4C+nvt7KNkmFibNzK02PcUI9PT4a9KqibYLyLGFm6YrBxub+rNL6SJC3UBKOCoqwQncqSj4oPbJjIToI35vMEaQSwhqFdxW48yqcJTb29TLzjK2MLipN7OST0s8mFhSkKTNKSJqEtgNv0g1ki32XQOUOidLYJqHv8zKeNPZD4qRpJAepj/QglaIwjJa+Hqa8CESKYj3mFbMBO8SSPWZnywGRCSLeklIlXDIzniz9MIpSMIcs0bgpjknIPsbwain2C5wvMo/sSI7kmMj8MCmM0lRCkiyHnu0vwWCCo5i7BL4xOy/i402YHAllm40ugmv40ZK/V9IJA9MPQ3/p2cyZkAIYrgUXJiY8CDPhIqYn2AD7IYMnm1DJNB/ZhmuqlmVkWc7mSsvYTDJHItWyLEKUXZv5GZ6W8c5OYbZvsmbjLAMfP80y9kXEeI5yfpLZWTGiWsZzN21tnyCbXZUpqpoGKGGXkZpzgDxDCg5kWROS5X7J5Gygul7uk6xmXqfva6mHiX/iDB2GWLY3WpDIu4WPSE2VFCtJUil+uIF3gRXM/YQW0nIlkSJMjhZy81DGEsrTBC+VaF5DFqlvzKQ99GKreXjDPewJU7tk7SC7WrNHRyBC4BfdOAeREYr2HNJaI8kucC4aoWHuJ8yeApuKrPL0M7XLIrgYKKn9oFTsLGMQUBA/pIx9BV7/au1q5JaaXrCfpIykCxv22l1kQnW8hyt2r6VThL7S71L+l/lbDsjDwD8F8BZaIu+3sZ46Wwl2ScnBh1VJmOlT7E2kLnxrISFNcWO2wha+DmuTKcAUEt1SeEwXh/BScgv+JiSAjpWpL21N5CACld57eNEltaMz8i6wUb7lXMOEQoSRAvI/LFG4A4tVNJOSD2aE9KZDw55ho1ztOvEl5+qxMfibLWSoJojNOIPXyFzQ4M6MUFgF8QK6goGNzJV5adh2lqMahA+VXGaOWZ8rdsPiwviqWrGVSQw4Teu19PvL0PI/gAEdaJJtb5hY0xZ+6kN1vHWh4hXeAUOrmpM4ORGO1oFNL/WzqyTDS0YJomAn2byox9hBpFia8blOIaws+/6W72lA/UO5ZHKaoYXpNYbXOly4QM/VZWlIcriyUfhpQNFu4uz5L/LOKB/QItitDxqkGy8ilDsMJnuvUc1k4zM2Usj3645Rh5a15mn/MN9laRjrHPGdAOpI8sZOMnZ4bTEPyaL+kYZkz3/rBZ//NrS4ZWhDzqi99j1iaufEga0XZx3s4GF1QEtJDlTCAi0DEaQ49oH3AvoERYpAUIEIifm7DR0huyLgueUDWhTa5ejpydlQZGd1wR2WOrddJrfrDxmFa4lpa8YEe/4qWLlktzLZPcFfAbTCjV0rOrxK27g4WX442HYhL88uqlRyCgKO62fQoWWX8FcODFYqXPJq8Ldi+6Q26vCEnDK/7HVHYcvg4qvAVsbfzVt2VjpmIu016BdgK4RxWG0WTG94GTIWRGdqHjOR4i4owrHFZBebNZLWzClfMwmjfwRMljLYcs4Mwl/UtbmkJ4SwdeAapv9hoiUJFgwtZQNdi1E0Q/JWSpk9cAXAa1BSbFoZj4ZXW8YOZJ0egUmXG2RT/yQhyCxkwsssIyaIkQ/zn0FTehzpjL8utV5lTEpa4LhEeyamkaMEbOF49NNCDs8JBjZmN/KYBe6zB0h8HDMjOedLS6ohlVyKmPx35z2v5i9DixkCB41NnKEalese2Ph9j31XG4qFdKXUZMTMflklSzss8+hSBWWZlUx06UhKmFi7MktG0vKKd47W85JnbNoVXBLRIjeYPXOp1NyqgDXcOquoiXLmhFRFRk2pYJrRLS4hM318Nt1JxTwMY59R3dqUEjKrq1ZS71Jpis7YoDTyAEmbS7Zh3FNFzMDy1/uw0ByplPeXWNcqSVUNZuWo+yoL6bXKFLTMjEq8t0r1HVUjZp5Vis0MNl+2vSLPUsb0Gr3AYMHyMvL+ToGh/Ysh/O9QKE+/N/Uf/aN/9I/+0e+k/wPnqqbBfHdoNwAAAABJRU5ErkJggg=="}
                  alt="Logo Ucaribe"
                />
                
              </Link>
              </a>
            </div>

            <nav className="hidden md:flex space-x-10">
              <div className="relative">
                <button
                  type="button"
                  className="text-gray-500 group bg-white rounded-md inline-flex items-center text-base font-medium hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                  aria-expanded="false"
                  //open the show

                  onClick={() => this.setState({ isOpen: !this.state.isOpen })}
                >
                  <span>Eventos </span>

                  <svg
                    className="text-gray-400 ml-2 h-5 w-5 group-hover:text-gray-500"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                    aria-hidden="true"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                </button>

                <div
                  className={`${
                    this.state.isOpen ? "absolute" : "hidden"
                  } z-10   -ml-4 mt-3 transform px-2 w-screen max-w-md sm:px-0 lg:ml-0 lg:left-1/2 lg:-translate-x-1/2 `}
                >
                  <div className="rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden">
                    <div className="relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8">
                      <a
                        href="#"
                        className="-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50"
                      >
                        <svg
                          className="flex-shrink-0 h-6 w-6 text-indigo-600"
                          xmlns="http://www.w3.org/2000/svg"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                          aria-hidden="true"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M9 19v-6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2a2 2 0 002-2zm0 0V9a2 2 0 012-2h2a2 2 0 012 2v10m-6 0a2 2 0 002 2h2a2 2 0 002-2m0 0V5a2 2 0 012-2h2a2 2 0 012 2v14a2 2 0 01-2 2h-2a2 2 0 01-2-2z"
                          />
                        </svg>
                        <Link href="/concursologo">
                        <div className="ml-4">
                          <p className="text-base font-medium text-gray-900">
                            Concurso de logotipo
                          </p>
                          <p className="mt-1 text-sm text-gray-500">
                            Espacio dedicado al concurso del logotipo del
                            Congreso de Estudiantes de Ingenieria
                          </p>
                        </div>
                        </Link>
                      </a>

                      
                    </div>
                  </div>
                </div>
              </div>

              <Link href="/gallery">
                <a className="text-base font-medium text-gray-500 hover:text-gray-900">
                  Galeria
                </a>
              </Link>
              <Link href="/agenda">
                <a className="text-base font-medium text-gray-500 hover:text-gray-900">
                  Agenda
                </a>
              </Link>

              <Link href="/entradas">
                <a className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-greend rounded-md shadow-sm text-base font-medium text-white bg-green-700 hover:bg-indigo-700">
                  Inscripciónes
                </a>
              </Link>
            </nav>
            <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
              <Link href="/register">
                <a className="whitespace-nowrap text-base font-medium text-gray-500 hover:text-gray-900">
                  Registrarse
                </a>
              </Link>

              <Link href="/login">
                <a className="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700">
                  Iniciar sesión
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
