import React, { Component } from "react";
import Slider from "react-slick";
import { Container } from "reactstrap";
/* Slider para las vistas de infomracion de tour y hoteles*/
function NextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{
        ...style,
        display: "block",
        background: "#CACACA",
        borderRadius: "50px",
      }}
      onClick={onClick}
    />
  );
}

function PrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{
        ...style,
        display: "block",
        background: "#CACACA",
        borderRadius: "50px",
      }}
      onClick={onClick}
    />
  );
}

export default class SimpleSlider extends Component {
  render() {
    const { slidesToShow = 1, settings: settings2 = {} } = this.props;
    const settings = {
      dots: false,
      infinite: true,
      fade: true,
      speed: 500,
      //adaptiveHeight: adaptiveHeight,
      slidesToShow: slidesToShow,
      slidesToScroll: 1,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />,
      ...settings2,
    };
    return (
      <Container>
        <Slider {...settings}>{this.props.children}</Slider>
      </Container>
    );
  }
}
