import React, { Component } from "react";
import { Container } from "reactstrap";

class CIVideo extends Component {
  render() {
    const {homeSiteInfo = {}} = this.props;
    return (
      <Container className="pt-4 text-center">
          {" "}
          <h1 style={{ textAlign: "center", fontSize: " 30px" }}>
            <b>Te esperamos en</b>
          </h1>
          <p style={{ textAlign: "center", fontSize: " 30px" }}>
            <b> El Congreso de Estudiantes de Ingenieria 2021</b>
          </p>
          <iframe
            style={{ width: "100%", height: "500px", borderRadius: "25px" }}
            src={homeSiteInfo.video}
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
          ></iframe>

        {/* slick here */}
      </Container>
    );
  }
}
export default CIVideo;
