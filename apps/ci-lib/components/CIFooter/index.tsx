import React, { Component } from "react";
import { Container } from "reactstrap";
class CIFooter extends Component {
  render() {
    return (
      <Container fluid className="m-0 p-0">
        <footer style={{ backgroundColor: "#2c292f" }}>
          <div className="container">
            <div className="row ">
              <div className="col-md-4 text-center text-md-left ">
                <div className="py-0">
                  <h3 className="my-4 text-white">
                    <span
                      className="mx-4 font-italic text-white "
                      style={{ fontSize: "25px" }}
                    >
                      Quienes Somos
                    </span>
                  </h3>
                  <div className="footer-links font-weight">
                    <b>
                      <a
                        className="text-white"
                        href="https://www.unicaribe.mx/mision-vision"
                        target="_blank"
                        style={{ fontSize: "18px" }}
                      >
                        Mision & Visión
                      </a>
                    </b>
                  </div>
                  <div className="footer-links font-weight">
                    <b>
                      <a
                        className="text-white"
                        href="https://www.unicaribe.mx/organigrama"
                        target="_blank"
                        style={{ fontSize: "18px" }}
                      >
                        Organigrama
                      </a>
                    </b>
                  </div>
                  <div
                    className="footer-links font-weight"
                    style={{ fontSize: "18px" }}
                  >
                    <b>
                      <a 
                      className="text-white"
                      href="https://www.unicaribe.mx/licenciaturas"
                      target="_blank"
                      >Programas Educativos</a>
                    </b>
                  </div>
                </div>
              </div>
              <div className="col-md-4 text-center text-md-left ">
                <div className="py-0">
                  <h3 className="my-4 text-white">
                    <span
                      className="mx-4 font-italic text-white "
                      style={{ fontSize: "25px" }}
                    >
                      {" "}
                      Congreso de Estudiantes
                    </span>
                  </h3>
                  <div
                    className="footer-links font-weight"
                    style={{ fontSize: "18px" }}
                  >
                    <b>
                      <a 
                      href="https://www.unicaribe.mx/aviso-privacidad"
                      target="_blank"
                      className="text-white"
                      >Aviso de Privacidad</a>
                    </b>
                  </div>               
                  <div
                    className="footer-links font-weight"
                    style={{ fontSize: "18px" }}
                  >
                    <b>
                      <a className="text-white">Seguridad</a>
                    </b>
                  </div>
                  <div
                    className="footer-links font-weight"
                    style={{ fontSize: "18px" }}
                  >
                    <b>
                      <a className="text-white">Garantia y Confianza</a>
                    </b>
                  </div>
                  
                  <p>
                    <a className="text-white mr-4" style={{ fontSize: "50px" }}>
                      <i className="fab fa-cc-visa"></i>
                    </a>
                    <a className="text-white mr-4" style={{ fontSize: "50px" }}>
                      <i className="fab fa-cc-mastercard"></i>
                    </a>
                  </p>
                </div>
              </div>

              <div className="py-0">
                  <h3 className="my-4 text-white">
                    <span
                      className="mx-4 font-italic text-white "
                      style={{ fontSize: "25px" }}
                    >
                      Contactanos
                    </span>
                  </h3>
                  <div
                    className="footer-links font-weight"
                    style={{ fontSize: "18px" }}
                  >
                    <b>
                      <i className="fa fa-phone  mx-2 "></i>
                      <a 
                      className="text-white"
                      href="https://www.unicaribe.mx/directorio"
                      target="_blank"
                      >Directorio</a>
                    </b>
                  </div>
                  <div
                    className="footer-links font-weight"
                    style={{ fontSize: "18px" }}
                  >
                    <b>
                      <i className="fa fa-envelope  mx-2"></i>
                      <span
                      className="text-white"
                      >hola@Congreso.com</span>
                    </b>
                  </div>
                  <div
                    className="footer-links font-weight"
                    style={{ fontSize: "18px" }}
                  >
                    <b>
                      <i className="fa fa-map-marker mx-2 "></i>
                      <a 
                      className="text-white"
                      href="https://www.unicaribe.mx/ubicacion"
                      target="_blank"
                      >Ubicación</a>
                    </b>
                  </div>

              </div>
            </div>
            <div className="row justify-content-md-center">
              <div className="col-md-4 text-white text-center">
                <div className="socials social-links mb-3">
                  <p className="mb-0">Siguenos en nuestras redes sociales</p>
                    <a
                      className="text-white mr-4"
                      style={{ fontSize: "24px" }}
                      href="https://www.facebook.com/congreso.ingenierias.9/"
                    >
                      <i className="fab fa-facebook"></i>
                    </a>

                    <a
                      className="text-white mr-4"
                      style={{ fontSize: "24px" }}
                      href="https://www.instagram.com/congresoingenierias/"
                    >
                      <i className="fab fa-instagram"></i>
                    </a>
                    <a
                      className="text-white mr-4"
                      style={{ fontSize: "24px" }}
                      href="https://www.youtube.com/channel/UC9HcX7Sxz--RxJF00o7EIYw/videos/"
                    >
                      <i className="fab fa-youtube" />
                    </a>
                </div>
              </div>
            </div>
            <div className="row justify-content-md-center">
              <div className="col-md-4 text-center">
                <p className="text-light">
                  &copy;2021 Congreso de Estudiantes de Ingenieria.
                </p>
              </div>
            </div>
          </div>
        </footer>
      </Container>
    );
  }
}
export default CIFooter;