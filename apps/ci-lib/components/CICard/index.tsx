import React from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
  Col,
} from "reactstrap";
import Link from "next/link";

const CICard = ({ Title, Description, img, url, isOffer = false }) => {
  return (
    <Col xs={12} sm={12} md={6} lg={3} className="mb-4">
      <Card>
        <CardImg top width="100%" src={img} alt="Card image cap" />
        <CardBody>
          <CardTitle tag="h5">{Title}</CardTitle>
          {isOffer ? <CardText>{Description}</CardText> : ""}
          <Link href={url}>
            <Button color="danger">Informacion</Button>
          </Link>
        </CardBody>
      </Card>
    </Col>
  );
};

export default CICard;
