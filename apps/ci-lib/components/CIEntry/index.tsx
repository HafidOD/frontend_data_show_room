import React, { Component } from "react";
import { Card, Row, Col, CardImg } from "reactstrap";
import AvailabilityInput from "../AvailabilityInput";
class CIEntry extends Component {
  render() {
    const { entryTitle, entryImg, quantity, access = [] } = this.props;
    return (
      <Card className="mb-4">
        <Row>
          <Col xs={6} md={6} lg={6}>
            <CardImg src={entryImg} style={{ height: "200px" }} />
          </Col>
          <Col xs={6} md={6} lg={6} className="pt-4">
            <strong>{entryTitle}</strong>
            <ul>
              {access.map((individualAccess) => (
                <li className="small" key={individualAccess}>
                  {individualAccess}
                </li>
              ))}
            </ul>
          </Col>
        </Row>
        <AvailabilityInput />
      </Card>
    );
  }
}

export default CIEntry;
