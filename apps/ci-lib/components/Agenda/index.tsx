import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import React, { Component } from "react";

class Agenda extends Component<{activities:Array<Object>}, { cards: any }> {
  render() {
    const {activities: cards} = this.props;
    return (
      <VerticalTimeline>
        {cards.map(({ title, subtitle, schedule, date }, index) => (
          <VerticalTimelineElement
            className="vertical-timeline-element--work"
            contentStyle={{ background: "rgb(0, 0, 0,8.4  )", color: "#fff" }}
            contentArrowStyle={{ borderRight: "7px solid  rgb(33, 150, 243)" }}
            date={date}
            iconStyle={{ background: "rgb(33, 150, 243)", color: "#fff" }}
            key={"timeLine-event" + index}
            //icon={<WorkIcon />}
          >
            <h3 className="vertical-timeline-element-title text-center mb-2">{title}</h3>
            <h4 className="vertical-timeline-element-subtitle">{subtitle}</h4>
            <table className="table text-white">
              <thead>
                <tr>
                  <td>Evento</td>
                  <td >Hora</td>
                  <td>Categoria</td>
                  <td>Limite</td>
                </tr>
              </thead>
              <tbody>
                {schedule.map((row, index) => (
                  <tr key={"row-event" + index}>
                    <td>{row.event}</td>
                    <td >{row.hour}</td>
                    <td>{row.category}</td>
                    <td>{row.accessLimit}</td>
                    
                  </tr>
                ))}
              </tbody>
            </table>
          </VerticalTimelineElement>
        ))}
        <VerticalTimelineElement
          iconStyle={{ background: "rgb(16, 204, 82)", color: "#fff" }}
          // icon={<StarIcon />}
        />
      </VerticalTimeline>
    );
  }
}
export default Agenda;
