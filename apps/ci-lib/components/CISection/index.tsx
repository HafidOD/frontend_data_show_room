import React, { Component } from "react";
import { Container, CardGroup } from "reactstrap";
import axios from "axios";
import CardComponent from "../CICard";
// import miImagen from '../../images/miimagen.png'
class CISection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: [],
      cardsOffers: [],
    };
  }
  componentDidMount() {
    const { endpoint } = ""; //this.props;

    //axios.get(`https://google.com/${endpoint}`).then((res)=>{
    // obtienes lo de la api  data retorne [{tours}]
    // const {data} = res;
    // this.setState({cards:data})

    this.setState({
      cards: [
        {
          id: 1,
          Title: "Todo sobre el mundo",
          url: "/conferencias/todo-sobre-el-mundo",
          img: "img/conferencia/todo/conf1.JPG",
        }, // img: miImagen
        {
          id: 2,
          Title: "ciencia cuantica",
          url: "/conferencias/ciencia-cuantica",
          img: "img/conferencia/todo/conf2.JPG",
        },
        {
          id: 3,
          Title: "Todo sobre el mundo",
          url: "/conferencias/todo-sobre-el-mundo",
          img: "img/conferencia/todo/conf1.JPG",
        }, // img: miImagen
        {
          id: 4,
          Title: "ciencia cuantica",
          url: "/conferencias/ciencia-cuantica",
          img: "img/conferencia/todo/conf2.JPG",
        },
      ],
      cardsOffers: [
        {
          id: 1,
          Title: "Origami",
          url: "/talleres/origami",
          img: "img/talleres/ori/ori1.JPG",
          Description: "Description",
        },
        {
          id: 2,
          Title: "Hacking",
          url: "/talleres/hacking",
          img: "img/talleres/hacking/hack1.JPG",
          Description: "Description",
        },
        {
          id: 3,
          Title: "Origami",
          url: "/talleres/origami",
          img: "img/talleres/ori/ori2.JPG",
          Description: "Description",
        },
        {
          id: 4,
          Title: "Hacking",
          url: "/talleres/hacking",
          img: "img/talleres/hacking/hack2.JPG",
          Description: "Description",
        },
      ],
    });
    //})
  }
  render() {
    const {
      sectionTitle = "",
      sectionDescription = "",
      isOffer = false,
    } = this.props;
    const { cards, cardsOffers } = this.state;
    return (
      <Container className="mt-4">
        <div className="text-center">
          <h2>{sectionTitle}</h2>
          <p>{sectionDescription}</p>
        </div>
        <CardGroup>
          {!isOffer
            ? cards.map((Card) => (
                <CardComponent
                  key={Card.id}
                  url={Card.url}
                  Title={Card.Title}
                  Description={Card.Description}
                  img={Card.img}
                />
              ))
            : ""}

          {isOffer
            ? cardsOffers.map((Card) => (
                <CardComponent
                  key={Card.id}
                  url={Card.url}
                  Title={Card.Title}
                  Description={Card.Description}
                  img={Card.img}
                  isOffer={true}
                />
              ))
            : ""}
        </CardGroup>
      </Container>
    );
  }
}
export default CISection;
