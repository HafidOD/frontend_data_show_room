import { Container } from "reactstrap";
import Slider from "ci-lib/components/Slider";
import style from "ci-lib/components/Patro/index.module.css";

export default function Patro(props) {
  const {images} = props;
  return (
    <Container className="pt-4 mt-4">
      <h2 className="text-center mb-2">Aliados {"&"} Patrocinadores</h2>
      <Slider settings={{ slidesToShow: 3, fade: false, responsive:[{breakpoint:900, settings:{slidesToShow:1}}] }}>
        {
          // response es de la api

          images.map((image) => (
            <img src={image} className={style.galleryImage} />
          ))
        }
      </Slider>
    </Container>
  );
}
