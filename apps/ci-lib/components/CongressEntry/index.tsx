import React from "react";
import Link from "next/link";
import { Col, Button } from "reactstrap";
// "http://via.placeholder.com/150"
export default class CongressEntry extends React.Component {
  render() {
    const { name, icon, include, price } = this.props;
    return (
      <Col xs={12} md={12} lg={4} className="text-center text-white">
        <h2>{name}</h2>
        <img className="rounded mx-auto d-block" src={icon} />
        <p className="text-white mt-3">{include}</p>
        <p className="text-success">
          <strong>{price}</strong>
        </p>
        
      </Col>
    );
  }
}
