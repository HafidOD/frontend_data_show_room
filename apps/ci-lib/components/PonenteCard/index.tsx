import React from "react";
import Link from "next/link";
import { Container } from "reactstrap";
// "http://via.placeholder.com/150"
export default class PonenteCard extends React.Component {
  render() {
    const { name, image, description } = this.props;
    return (
      <Container fluid className="text-center pt-4">
        <img
          src={image} //"https://vd5n1g.dm.files.1drv.com/y4mLLt8TGMzr7wlAZ2I8GalH63lmA4RcZQkdWdLkZb9FT32F_EIZp6EoiCegHB7td-j_f1web7tosfrPt70ixNo2V10eABoDKRmLQ71zwh-n8e1yFwY29L16Li-JVKPa2RmfhQ1MCEK2yhdZ9KZkuACHcBSJ8Dmcp10MpkgloUzcVislHmWmd-pI0Gn8_P5-NC6Ix_7Y59LzKyZ2Gpig52m3g/IMG_4544.JPG?psid=1"
          className="mx-auto d-block"
          style={{ borderRadius: "25px", width:"250px", height:"250px"}}
          alt="..."
        />
        <p className="pt-3">
          <strong>{name}</strong>
        </p>
        <p>{description}</p>
      </Container>
    );
  }
}
