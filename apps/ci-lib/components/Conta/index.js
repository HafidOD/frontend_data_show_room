import React from "react";
import { Container, Row, Col } from "reactstrap";
import { CountdownCircleTimer } from "react-countdown-circle-timer";
import "moment/locale/tr";
import Clock from "react-live-clock";
import "moment-timezone";
import "react-moment";

const minuteSeconds = 60;
const hourSeconds = minuteSeconds * 60;
const daySeconds = hourSeconds * 24;

const timerProps = {
  isPlaying: true,
  size: 90,
  strokeWidth: 6,
};

const renderTime = (time) => {
  return (
    <div className="time-wrapper">
      <div className="title time">{time}</div>
    </div>
  );
};

const getTimeSeconds = (time) => Math.ceil(minuteSeconds - time) - 1;
const getTimeMinutes = (time) => Math.ceil(time / minuteSeconds) - 1;
const getTimeHours = (time) => Math.ceil(time / hourSeconds) - 1;
const getTimeDays = (time) => Math.ceil(time / daySeconds) - 1;
function convertDateToFecha(date){
  const fecha = date.slice(0,10)
  const parts = fecha.split('-')
  let month = "";
  switch (parts[1]){
    case '01': month = "de Enero del"; break;
    case '02': month = "de Febrero del"; break;
    case '03': month = "de Marzo del"; break;
    case '04': month = "de Abril del"; break;
    case '05': month = "de Mayo del"; break;
    case '06': month = "de Junio del"; break;
    case '07': month = "de Julio del"; break;
    case '08': month = "de Agosto del"; break;
    case '09': month = "de Septiembre del"; break;
    case '10': month = "de Octubre del"; break;
    case '11': month = "de Noviembre del"; break;
    case '11': month = "de Diciembre del"; break;
  }
  return `${parts[2]} ${month} ${parts[0]}`
}
export default function CountDown(props) {
  const {homeSiteInfo={}} = props;
  const {congressDate="2021-11-11"} = homeSiteInfo;
  const startTime = Date.now() / 1000; // use UNIX timestamp in seconds
  const endTime = new Date(congressDate) / 1000; // use UNIX timestamp in seconds startTime + 143201
  const remainingTime = endTime - startTime;
  const days = Math.ceil(remainingTime / daySeconds);
  const daysDuration = days * daySeconds;
  /*8CBFF0 BC8CF0 #D9B5FF*/
  const bgColor ="#333"
  return (
    <Container
      fluid
      className="pt-4 pb-4 text-white "
      style={{ backgroundColor: bgColor }}
    >
      <section className="countdown text-center mt-4">
        <div className="to-calendar">
          <h3 className="title r-time">
            
            ¡Te esperamos el {convertDateToFecha(congressDate)}!
          </h3>
        </div>

        <div className="all-times-bar text-center row justify-content-center mt-4">
          <div className="timebox">
            <div className="day ml-3">
              <CountdownCircleTimer
                {...timerProps}
                colors={[
                  ["#9000ff", 0],
                  ["#0066FF", 1],
                ]}
                isLinearGradient={true}
                duration={daysDuration}
                initialRemainingTime={remainingTime}
                trailColor={[["#dbdbdb"]]}
              >
                {({ elapsedTime }) =>
                  renderTime(getTimeDays(daysDuration - elapsedTime))
                }
              </CountdownCircleTimer>
            </div>
            <h3 className="title text-center">Dias</h3>
          </div>

          <div className="timebox">
            <div className="hours ml-3">
              <CountdownCircleTimer
                {...timerProps}
                colors={[
                  ["#9000ff", 0],
                  ["#0066FF", 1],
                ]}
                isLinearGradient={true}
                duration={daySeconds}
                initialRemainingTime={remainingTime % daySeconds}
                onComplete={(totalElapsedTime) => [
                  remainingTime - totalElapsedTime > hourSeconds,
                ]}
                trailColor={[["#dbdbdb"]]}
              >
                {({ elapsedTime }) =>
                  renderTime(getTimeHours(daySeconds - elapsedTime))
                }
              </CountdownCircleTimer>
            </div>
            <h3 className="title">Horas</h3>
          </div>
          <div className="timebox">
            <div className="minutes ml-3">
              <CountdownCircleTimer
                {...timerProps}
                colors={[
                  ["#9000ff", 0],
                  ["#0066FF", 1],
                ]}
                isLinearGradient={true}
                duration={hourSeconds}
                initialRemainingTime={remainingTime % hourSeconds}
                onComplete={(totalElapsedTime) => [
                  remainingTime - totalElapsedTime > minuteSeconds,
                ]}
                trailColor={[["#dbdbdb"]]}
              >
                {({ elapsedTime }) =>
                  renderTime(getTimeMinutes(hourSeconds - elapsedTime))
                }
              </CountdownCircleTimer>
            </div>
            <h3 className="title">Minutos</h3>
          </div>
        </div>
      </section>
    </Container>
  );
}
