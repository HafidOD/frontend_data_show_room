import { Container } from "reactstrap";
import CIHeader from "ci-lib/components/CIHeader";
import CIFooter from "ci-lib/components/CIFooter";

export default function Layout(props) {
  return (
    <Container fluid className={`m-0 p-0 text-white ${props.className}`}>
      <CIHeader homeSiteInfo={props.homeSiteInfo} />
      {props.children}
      <CIFooter />
    </Container>
  );
}
