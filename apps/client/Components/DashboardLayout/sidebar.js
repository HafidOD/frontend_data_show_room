import React from "react";
import Link from "next/link";
import { useSession } from 'next-auth/client'
function sidebar() {
  const [ session, loading ] = useSession()
  return (
    <>
      <div className="vertical-nav bg-white" id="sidebar">
        <div className="py-4 px-3 mb-4 bg-light">
          <div className="media d-flex align-items-center">
            {/* <img
              src=""
              alt="..."
              width="80"
              height="80"
              className="mr-3 rounded-circle img-thumbnail shadow-sm"
            <img
        />*/}
            <div className="media-body">
              <h4 className="m-0">{session.user.name}</h4>
              <p className="font-weight-normal text-muted mb-0">{session.user.email}</p>
            </div>
          </div>
        </div>
        <p className="text-gray font-weight-bold text-uppercase px-3 small pb-4 mb-0">
          Dashboard
        </p>

        <ul className="nav flex-column bg-white mb-0">
          <li className="nav-item">
            <Link href="/editregister">
              <a className="nav-link text-dark bg-light">
                <i className="fas fa-arrow-alt-circle-right mr-3 text-primary fa-fw"></i>{" "}
                Registro
              </a>
            </Link>
          </li>
         
         
         
          <li className="nav-item">
            <Link href="#">
              <a className="nav-link text-dark">
                <i className="fas fa-ticket-alt mr-3 text-primary fa-fw"></i>{" "}
                Inscripción
              </a>
            </Link>
          </li>
        </ul>
        <p className="text-gray font-weight-bold text-uppercase px-3 small py-4 mb-0">
          Charts
        </p>

        <ul className="nav flex-column bg-white mb-0">
          
          <li className="nav-item">
            <Link href="/">
              <a className="nav-link text-dark">
                <i className="fas fa-sign-out-alt mr-3 text-primary fa-fw"></i>
                Cerrar Sesion
              </a>
            </Link>
          </li>
        </ul>
      </div>
    </>
  );
}

export default sidebar;
