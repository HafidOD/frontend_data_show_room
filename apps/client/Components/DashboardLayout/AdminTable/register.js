import React, { Component } from "react";
import api from "../../utils/api";
//import ButtonSidebar from "../components/buttonSidebarCollapse";

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  async submitForm() {
    const body = { email: this.state.email, password: this.state.password };
    try{
      await api.admin.register(body);
      alert("User Registered")
      this.setState({ email: "", password: "" });
    } catch(err){
      alert(err.response.data.message)
    }
  }

  render() {
    return (
      <div className="page-content p-5" id="content">
        <h2>Registrar Usuario Admin</h2>
        <form className="mt-4">
          <div className="">
            <label>Email</label>
            <input
              name="email"
              className="form-control"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Contraseña</label>
            <input
              name="password"
              type="password"
              className="form-control"
              value={this.state.password}
              onChange={this.handleChange}
            />
          </div>
        </form>
        <button
          type="button"
          onClick={() => this.submitForm()}
          className="btn btn-success text-uppercase mt-4"
        >
          Agregar
        </button>
      </div>
    );
  }
}

export default Table;
