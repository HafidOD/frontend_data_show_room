import React, { Component } from "react";
import api from "../../utils/api";
//import ButtonSidebar from "../components/buttonSidebarCollapse";

class Table extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: "",
      image: "",
      partners: [],
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.syncPartners = this.syncPartners.bind(this);
    this.deletePartner = this.deletePartner.bind(this);
    this.toggleEditMode = this.toggleEditMode.bind(this);
    this.handleEditChange = this.handleEditChange.bind(this);
  }
  async syncPartners() {
    const { results } = await api.partners.get();
    this.setState({ partners: results });
  }
  async componentDidMount() {
    await this.syncPartners();
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleEditChange(id, field, value) {
    let partners = this.state.partners;

    const partnerId = this.state.partners.findIndex(
      (partner) => partner.id == id
    );
    switch (field) {
      case "url":
        partners[partnerId].url = value;
        break;
      case "image":
        partners[partnerId].image = value;
        break;
      default:
        break;
    }
    this.setState({ partners: partners });
  }
  async submitForm() {
    const body = { url: this.state.url, image: this.state.image };
    await api.partners.post(body);
    this.setState({ url: "", image: "" });
    await this.syncPartners();
  }

  async deletePartner(id) {
    await api.partners.remove(id);
    await this.syncPartners();
  }

  toggleEditMode(id) {
    const partnersEditList = this.state.partners.map((partner) => {
      if (partner.id == id) {
        partner.editMode = partner.editMode ? false : true;
        if (partner.editMode == false) {
          api.partners.put(id, partner);
          alert("Updated (=");
          this.syncPartners();
        }
      }
      return partner;
    });
    this.setState({ partners: partnersEditList });
  }

  render() {
    return (
      <div className="page-content p-5" id="content">
        {/*<ButtonSidebar />*/}
        <table className="table table-bordered">
          <thead className="thead-dark">
            <tr style={{ textAlign: "center" }}>
              <th scope="col">id</th>
              <th scope="col">Patrocinadores</th>
              <th scope="col">URL</th>
              <th scope="col">options</th>
            </tr>
          </thead>
          <tbody>
            {this.state.partners.map((partner, index) => (
              <tr key={partner.id} style={{ textAlign: "center" }}>
                <th scope="row">{index + 1}</th>
                <td>
                  {partner.editMode ? (
                    <input
                      type="text"
                      defaultValue={partner.url}
                      onChange={(e) =>
                        this.handleEditChange(partner.id, "url", e.target.value)
                      }
                    />
                  ) : (
                    partner.url
                  )}
                </td>
                <td>
                  {partner.editMode ? (
                    <input
                      type="text"
                      defaultValue={partner.image}
                      onChange={(e) =>
                        this.handleEditChange(
                          partner.id,
                          "image",
                          e.target.value
                        )
                      }
                    />
                  ) : (
                    partner.image
                  )}
                </td>

                {partner.editMode ? (
                  <td>
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(partner.id)}
                      className="btn btn-info"
                    >
                      Guardar
                    </button>
                  </td>
                ) : (
                  <td>
                    <button
                      type="button"
                      onClick={() => this.deletePartner(partner.id)}
                      className="btn btn-danger"
                    >
                      eliminar
                    </button>{" "}
                    <button
                      type="button"
                      onClick={async () => this.toggleEditMode(partner.id)}
                      className="btn btn-primary"
                    >
                      editar
                    </button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
        <form>
          <div className="">
            <label>Url Patrocinador</label>
            <input
              name="url"
              className="form-control"
              value={this.state.url}
              onChange={this.handleChange}
            />
          </div>
          <div className="">
            <label>Url Imagen</label>
            <input
              name="image"
              className="form-control"
              value={this.state.image}
              onChange={this.handleChange}
            />
          </div>
        </form>
        <button
          type="button"
          onClick={() => this.submitForm()}
          className="btn btn-success text-uppercase mt-4"
        >
          Agregar
        </button>
      </div>
    );
  }
}

export default Table;
