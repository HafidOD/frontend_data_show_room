import React, { Component } from 'react';
import axios from 'axios';

export default class paymentForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      url: ""
    }
  }

  async componentDidMount(){
    try{

      const {ticketId, userIdentification} = this.props;
      // Tu vas a mandar el user_identification y el ticket a ingresas
      let {data} = await axios.get('http://localhost:3000/api/mercadopago');
      this.setState({ url: data.init_point});
    } catch (ex) {
      console.log(ex)
    }
  }
  render(){
    const {url} = this.state;
    return url !== "" ? (
      <iframe style={{width:"100%", minHeight:"100vh"}} src={url}></iframe>
    ):null;
  }
}