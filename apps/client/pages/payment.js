import {useState, useEffect} from 'react'
import MercadoPagoForm from '../Components/MercadoPagoForm'
import { Container } from "reactstrap";
import api from '../utils/api'
import Layout from '../Components/Layout';

export default function app(props){
  const [homeSiteInfo, setHomeSiteInfo] = useState();

  useEffect(() => {
    async function getHomeSite() {
      const {results:response} = await api.homesite.get();
      setHomeSiteInfo(response)
    };
    getHomeSite()
  },[])
  // AQUI TU VAS A MANDARLE AL COMPONENT MERCADOPAGOFORM que ticket selecciono el usuario, lo puedes poner en la url y mandarlo 
  return (
    <Layout className="bg-pattern" homeSiteInfo={homeSiteInfo} >
    <Container className="pt-4 pb-4 mt-4 mb-4 bg-dark-opacity rounded index-container" >
    <MercadoPagoForm /* aqui ticketId={alguna variable}   userIdentification={otra_variable} *//>
    </Container>
    </Layout>  
  )
}
