import React from "react";
import Link from "next/link";
import CIHeader from "ci-lib/components/CIHeader";
import Layout from "../Components/Layout";
import Slider from "ci-lib/components/Slider";
import { Container } from "reactstrap";
import { CONNREFUSED } from "node:dns";
export default class Gallery extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const bgColor ="#333"
    return (
      
      <Layout className="bg-pattern">
        {/*
        response = lo que devuelva la api
          response.map(({year,imgs})=>(
            <Container className="pt-4 mt-4">
              <h2 className="text-center mb-2">{year}</h2>
              <Slider>
               {imgs.map(img)=>(
                 <img
                    src="https://vd5n1g.dm.files.1drv.com/y4mLLt8TGMzr7wlAZ2I8GalH63lmA4RcZQkdWdLkZb9FT32F_EIZp6EoiCegHB7td-j_f1web7tosfrPt70ixNo2V10eABoDKRmLQ71zwh-n8e1yFwY29L16Li-JVKPa2RmfhQ1MCEK2yhdZ9KZkuACHcBSJ8Dmcp10MpkgloUzcVislHmWmd-pI0Gn8_P5-NC6Ix_7Y59LzKyZ2Gpig52m3g/IMG_4544.JPG?psid=1"
                    className="card-img-top"
                    alt="..."
                  />
               )}
              </Slider>
            </Container>
          ))
        */}
        <Container  fluid
      className="pt-4 pb-4 text-white "
      style={{ backgroundColor: bgColor }}>
       
        <h1
          className="text-center mb-2"
          style={{ color: "whitte", padding: "10px" }}
        >
          GALERIA DEl CONGRESO DE ESTUDIANTES DE INGENIERIA.
        </h1>
        </Container>
        <Container className="pt-4 mt-4" style={{ maxWidth: "55vw" }} >
          <Container fluid
      className="pt-4 pb-4 text-white "
      style={{ backgroundColor: bgColor }} > 
          <h2 className="text-center mb-2" style={{ color: "whitte" }}>
            {" "}
            <b>CONGRESO 2020</b>
          </h2>
          </Container>
          <Slider>
            <img
              src="img/talleres/hacking/hack1.JPG"
              className="card-img-top"
              alt="..."
            />
            <img
              src="img/talleres/hacking/hack2.JPG"
              className="card-img-top"
              alt="..."
            />
            <img
              src="img/talleres/ori/ori2.JPG"
              className="card-img-top"
              alt="..."
            />
            <img
              src="img/talleres/ori/ori3.JPG"
              className="card-img-top"
              alt="..."
            />
            <img
              src="img/talleres/ori/ori1.JPG"
              className="card-img-top"
              alt="..."
            />
          </Slider>
        </Container>
        <Container className="pt-4 mt-4" style={{ maxWidth: "55vw" }} >
          <Container fluid
      className="pt-4 pb-4 text-white "
      style={{ backgroundColor: bgColor }}>
          <h2 className="text-center mb-2" style={{ color: "WHITTE" }}>
            <b> CONGRESO 2019</b>
          </h2>
          </Container>

          <Slider>
            <img
              style={{ borderRadius: "25px" }}
              src="img/conferencia/todo/conf1.JPG"
              className="card-img-top"
              alt="..."
            />
            <img
              src="img/conferencia/todo/conf2.JPG"
              className="card-img-top"
              alt="..."
            />
            <img
              style={{ borderRadius: "25px" }}
              src="img/talleres/map/map1.JPG"
              className="card-img-top"
              alt="..."
            />
            <img
              src="img/talleres/map/map2.JPG"
              className="card-img-top"
              alt="..."
            />
          </Slider>
        </Container>
      </Layout>
    );
  }
}
/**
 * <div className="card m-3" style={{ width: "18rem" }}>
          <img
            src="https://vd5n1g.dm.files.1drv.com/y4mLLt8TGMzr7wlAZ2I8GalH63lmA4RcZQkdWdLkZb9FT32F_EIZp6EoiCegHB7td-j_f1web7tosfrPt70ixNo2V10eABoDKRmLQ71zwh-n8e1yFwY29L16Li-JVKPa2RmfhQ1MCEK2yhdZ9KZkuACHcBSJ8Dmcp10MpkgloUzcVislHmWmd-pI0Gn8_P5-NC6Ix_7Y59LzKyZ2Gpig52m3g/IMG_4544.JPG?psid=1"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
        <div className="card m-3" style={{ width: "18rem" }}>
          <img
            src="https://vd5n1g.dm.files.1drv.com/y4mRP2JhRg_2UPWkDzgw36Z0jDs7vOQkr9mxsRcVkrQYcOBhO9_4zstHU-0vrBcZ6zP942uTEp0VeF9r0lMTb3m2BDmqDSEgYiIi5B0vjGvk8Qono2jJyRstNbRHyRGBYAZdjkzbH-xtWjXsbwCwMrtRXZgBMMLt5TkvTdGwJhh9kHLCl4Vyk-MZu0iTO3zQtCoB1bEwhCRR4a4iwPujhml8w/IMG_4547.JPG?psid=1"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
        <div className="card m-3" style={{ width: "18rem" }}>
          <img
            src="https://vd5n1g.dm.files.1drv.com/y4mTkoMDMmo-4FvOTM8U1qMQzdyq2ICr2hvN8L0WEf8p7SBR0UfLUaHIeK_qC3mSA9lOdHXU10hUw6BwdAeZhByhvgo0o-L8FpBW6FENjdZWztHzZfy-prmSDnac4Ok1SinfiYmg591xsDl8MncyvQ0CMYxNZwXD5eZys_AvrnAPyzweaa6W3Wo5a-3x_aXhEiE_6tXipd9xTAOb2aoA45CQQ/IMG_4636.JPG?psid=1"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>
        <div className="card m-3" style={{ width: "18rem" }}>
          <img
            src="https://vd5n1g.dm.files.1drv.com/y4m3le6XA7qRacJvVbsB1qwvwUlAfo6VVNAs_MpdxddmJqxFAuVY4AClw9Fji_Vc6wTRF0n1pfHXkBU-0ohIwgkkmzVTp22XzWgfz1-9TC-Yuiw8smuXSgz8--5z1EOLzrGdotY9mKnv4xSa1EaTfYnY4WJebOZQKMB0_0CyHXOuokABeN1JTXoifGvLVqCSRwxizNicTDo4K9ESE25AL8aFQ/IMG_4651.JPG?psid=1"
            className="card-img-top"
            alt="..."
          />
          <div className="card-body">
            <h5 className="card-title">Card title</h5>
            <p className="card-text">
              Some quick example text to build on the card title and make up the
              bulk of the card's content.
            </p>
            <a href="#" className="btn btn-primary">
              Go somewhere
            </a>
          </div>
        </div>

        <div>
          <div className="card" style={{ width: "18rem" }}>
            <img src="..." className="card-img-top" alt="..." />
            <div className="card-body">
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
            </div>
          </div>
          <div className="card" style={{ width: "18rem" }}>
            <img src="..." className="card-img-top" alt="..." />
            <div className="card-body">
              <p className="card-text">
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </p>
            </div>
          </div>
        </div>

 */
