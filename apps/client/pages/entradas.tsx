import React from "react";
import Link from "next/link";
import CIentry from "ci-lib/components/CongressEntry";
import Layout from '../Components/Layout';
import { Container, Row, Col, Button } from "reactstrap";
import api from '../utils/api'
export default class EntradasPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      entries:[]
    }
  }
  async componentDidMount(){
    const {results: entries} = await api.tickets.get();
    const mappedEntries = entries.map(entry =>({
      name: entry.title,
      icon: entry.image,
      include: entry.description,
      price: `$${Number(entry.price).toFixed(2)}`,
    }))
    this.setState({entries:mappedEntries})
  }
  render() {
    const {entries} = this.state;
    return (
      <Layout className="bg-pattern">
        {/** This is bg */}
        <Container fluid color="success" className="bg-dark pt-4 pb-4">
          {/** This is content */}
          <Container
            className="pt-4 pb-4"
            style={{ backgroundColor: "rgba(0,0,0,0.3)" }}
          >
            <Row>
              {entries.map(entry=><CIentry {...entry} />)}
            </Row>
          </Container>
        </Container>
      </Layout>
    );
  }
}
