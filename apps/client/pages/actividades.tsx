import React from "react";
import Link from "next/link";
import Agenda from "ci-lib/components/Agenda";
import Layout from "../Components/Layout";
import api from '../utils/api'
interface IProps {}

interface IState {
  isOpen: boolean;
  activities: Array<object>;
}

export default class AgendaPage extends React.Component<IProps, IState> {
  constructor(props) {
    super(props);
    this.state = { 
      isOpen: false, 
      activities:[] 
    };
    this.mapActivities = this.mapActivities.bind(this)
  }
  mapActivities(results){
    let dates=[]
    results.forEach(activity=>{
      if (dates[activity.date] == undefined){
        dates[activity.date] = []
      }
      dates[activity.date].push(activity)
    })
    let agenda = []
    const Keys = Object.keys(dates);
    Keys.forEach((date,day)=>{
      const schedule = dates[date].map(date=>({event: date.name, hour: date.schedule, category: date.name}))
      const obj = {
        title: "DIA " + (day+1),
        schedule,
        date: date.toString().slice(0,10)
      }
      agenda.push(obj)
    })

    this.setState({activities: agenda})
  }
  async componentDidMount(){
    const {results} = await api.activities.get();
    this.mapActivities(results);
  }
  render() {
    return (
      <Layout className="bg-pattern">
      
        <Agenda activities={this.state.activities}/>
    
      </Layout>
    );
  }
}
