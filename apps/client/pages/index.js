import { useState, useEffect } from 'react';
import { Container } from "reactstrap";
import Carousel from "ci-lib/components/Carousel";
import Slider from "ci-lib/components/Slider";
import PonenteCard from "ci-lib/components/PonenteCard";
import CIVideo from "ci-lib/components/CIVideo";
import Patro from "ci-lib/components/Patro";
import Reloj from "ci-lib/components/Conta";
import Layout from '../Components/Layout';
import api from '../utils/api'
export default function Home() {
  const [ponents, setPonents] = useState([]);
  useEffect(() => {
    async function getPonents() {
      const {results:response} = await api.ponents.get();
      const ponentsMapped = response.map(ponent => ({
        ...ponent,
        name: `${ponent.firstName} ${ponent.lastName}`,
      }))
      setPonents(ponentsMapped)
    };
    getPonents()
  },[])
  const [images, setImages] = useState([]);
  useEffect(() => {
    async function getPartnerships() {
      const {results:response} = await api.partners.get();
      const images = response.map(partner => partner.image);
      setImages(images)
    };
    getPartnerships()
  },[])
  // para obtener el logo desde la api 
  const [homeSiteInfo, setHomeSiteInfo] = useState();

  useEffect(() => {
    async function getHomeSite() {
      const {results:response} = await api.homesite.get();
      setHomeSiteInfo(response)
    };
    getHomeSite()
  },[])
  // -----------------
  return (
    <Layout className="bg-pattern" homeSiteInfo={homeSiteInfo} >
      <Carousel homeSiteInfo={homeSiteInfo} />
      <Reloj homeSiteInfo={homeSiteInfo} />
      <Container className="pt-4 pb-4 mt-4 mb-4 bg-dark-opacity rounded index-container" >
        <CIVideo homeSiteInfo={homeSiteInfo}/>
        <hr className="mt-4 mb-4"/>
        <h2 className="text-center mb-2">Ponentes</h2>
        <Slider settings={{ slidesToShow: 3, fade: false, responsive:[{breakpoint:900, settings:{slidesToShow:1}}] }}>
          {
            // response es de la api
            ponents.map((ponente) => (
              <PonenteCard {...ponente} />
            ))
          }
        </Slider>
        <hr className="mt-4 mb-4"/>
        <Patro images={images}/>
      </Container>
    </Layout>
  );
}
