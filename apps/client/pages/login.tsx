import React from "react";
import Link from "next/link";
import Layout from '../Components/Layout'
import { signIn } from 'next-auth/client'

export default class CIHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      email: '', 
      password:'' 
    };
    this.loginAction = this.loginAction.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e){
    this.setState({[e.target.name]: e.target.value});
  }
  loginAction(e){
    e.preventDefault();
    signIn('credentials', {...this.state, callbackUrl: 'http://localhost:3000/editregister'})
  }
  render() {
    return (
      <Layout className="bg-pattern">

        <div className="container mt-3 mb-4">
          <div className="card mx-auto col-4 p-3">
            <form onSubmit={this.loginAction}>
              <h3 style={{color:"black"}}>Log in</h3>
              <div className="form-group">
                <label style={{color:"black"}}>Email</label>
                <input
                  type="email"
                  name="email"
                  className="form-control"
                  placeholder="Enter email"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label style={{color:"black"}}>Password</label>
                <input
                  type="password"
                  name="password"
                  className="form-control"
                  placeholder="Enter password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </div>
              <button type="submit" className="btn btn-dark btn-block" >
                Sign in
              </button>
            </form>
          </div>
        </div>
      </Layout>
    );
  }
}
