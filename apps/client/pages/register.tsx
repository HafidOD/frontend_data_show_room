import React from "react";
import Link from "next/link";
import Layout from '../Components/Layout'
import axios from "axios";

export default class CIHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = { firstName:'', lastName:'', email:'', password:'', major:'' };
    this.submitForm = this.submitForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e){
    this.setState({[e.target.name]: e.target.value});
  }
  async submitForm(e){
    e.preventDefault();
    try{
        const user = await axios.post('http://localhost:7000/userAuth/register', this.state);
        alert("success")
    } catch (err) {
      alert(err)
    }
  }
  render() {
    return (
      <Layout className="bg-pattern">
        <div className="container mt-3" style={{padding:"15px"}}>
          <div className="card mx-auto col-4 p-3">
            <form onSubmit={this.submitForm}>
              <h3 style={{color:"black"}}>Registro</h3>

              <div className="form-group">
                <label style={{color:"black"}}>Nombre(s)</label>
                <input
                  type="text"
                  name="firstName"
                  className="form-control"
                  placeholder="Nombre(s)"
                  value={this.state.firstName}
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label style={{color:"black"}}>Apellido(s)</label>
                <input
                  name="lastName"
                  type="text"
                  className="form-control"
                  placeholder="Apellido(s)"
                  value={this.state.lastName}
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label style={{color:"black"}}>Correo</label>
                <input
                  name="email"
                  type="email"
                  className="form-control"
                  placeholder="Inserte Correo"
                  value={this.state.email}
                  onChange={this.handleChange}
                />
              </div>

              <div className="form-group">
                <label style={{color:"black"}}>Contraseña</label>
                <input
                  name="password"
                  type="password"
                  className="form-control"
                  placeholder="Inserte Contraseña"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-group">
                <label style={{color:"black"}}>Carrera</label>
                <input
                  name="major"
                  type="major"
                  className="form-control"
                  placeholder="Carrera"
                  value={this.state.major}
                  onChange={this.handleChange}
                />
              </div>
              <button type="submit" className="btn btn-dark btn-lg btn-block">
                Registrar
              </button>
             
             <Link href="/login"> 
              <p  type="submit"  className="forgot-password text-right" style={{color:"black"}}>
                ya estas registrado <a style={{color:"red"}}>log in?</a>
              </p></Link>
            </form>
          </div>
        </div>
      </Layout>

    );
  }
}
