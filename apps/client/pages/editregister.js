import React from "react";
import DashboardLayout from '../Components/DashboardLayout/Layout'
import Table from "../Components/UserTable/register";

const Home = () => {
  return <DashboardLayout>
    <Table />
    </DashboardLayout>;
};

export default Home;
