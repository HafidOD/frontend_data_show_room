import React from "react";
import Link from "next/link";
import CIHeader from "ci-lib/components/CIHeader";
import Layout from "../Components/Layout";
import Slider from "ci-lib/components/Slider";
import { Container } from "reactstrap";
import { CONNREFUSED } from "node:dns";
export default class concursologo extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const bgColor ="#333"
    const bgColorr ="white"
    return (
      
      <Layout className="bg-pattern">
       <Container style={{padding:"20px"}}>
                   <Container  fluid
      className="pt-4 pb-4 text-white "
      style={{ backgroundColor: bgColor , margin:"4px"}}>
       
        <h1
          className="text-center mb-2"
          style={{ color: "whitte", padding: "10px" }}
        >
         CONVOCATORIA  LOGO
        </h1>
        
        </Container>     
        <Container  
      className="pt-2 pb-2 text-white "
      style={{ backgroundColor: bgColorr }}>
        <h2 style={{textAlign:"center", fontSize:"28px", color:"black"}}>  DISEÑO DE LOGOTIPO y LEMA DEL CONGRESO DE ESTUDIANTES DE INGENIERÍA(CEI)   </h2>
        <Container style={{padding:"10px", color:"black"}}>
            <h2 style={{textAlign:"justify" , fontSize:"18px"}}>
            <b>El Departamento de Ciencias Básicas e Ingenierías convoca a la comunidad
universitaria, a presentar propuestas para el diseño del logotipo (logo) y lema del
CEI  de acuerdo con las siguientes:</b>
            </h2>
            <p style={{padding:"9px"}}>
        <h1 style={{textAlign:"center", fontSize:"35px", color:"black"}}><b>BASES</b></h1>

            </p>

            <p>
        <h2 style={{textAlign:"left", fontSize:"20px"}}>

        <b>1. Objetivo del concurso.</b>
        </h2>
         <h3 style={{textAlign:"justify", fontSize:"18px"}}>  
         I. Diseñar el logo que distinguirá al CEI , el cual deberá consistir en una
imagen original e inédita que tome en consideración como elementos
primordiales para la ejecución del diseño, la ingeniería, la sociedad, las
tendencias y los valores que promueve la Universidad.
II. Proponer un lema que no cuente con registro de propiedad intelectual, frase
corta, en lenguaje claro y sencillo que invite a la acción, que promueva la
participación en el CEI  y que refleje el espíritu del mismo.
 </h3>
<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>2. Categorías </b>
  </h2>


  <h3 style={{textAlign:"justify" ,fontSize:"18px"}}> 

I. Diseño del logo
<p>II. Lema</p>
</h3>
<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>3. Características.</b>
  </h2>


  <h3 style={{textAlign:"justify", fontSize:"18px"}} > 

  <p>I. El participante podrá presentar solo una propuesta de forma individual en
una o en ambas categorías.</p>
<p>II. Pueden presentar una propuesta en equipo (máximo 3 integrantes).</p>
<p>III. En caso de que el participante presente una propuesta en equipo, no podrá
participar de forma individual.</p>
<p>IV. El desarrollo del diseño del logo y el lema estará enfocado a destacar a las
Ingenierías que se imparten en la Universidad, de manera integradora y
equilibrada.  </p>
<p> V. Las propuestas deberán ser originales, inéditas, no haber sido presentadas
en otro concurso ni contar con registro de propiedad intelectual.</p>
<p> VI. Los formatos para entrega del logo participante deberán ser: archivo
vectorizado .AI (Adobe Ilustrator), .PDF (Adobe Acrobat) y .TIFF (300dpi).</p>
<p> Los formatos para entrega del lema participante deberán ser: archivo
.DOCX (Word) o .PDF (Adobe Acrobat).</p>
<p> VIII. Los trabajos deberán acompañarse de una breve descripción del diseño del
logo, en una extensión máxima de una cuartilla.</p>
<p> IX. Los participantes deberán guardar copia del material enviado.</p>
<p> X. Se excluirán todos aquellos logos que tengan connotaciones sexistas,
xenófobas, racistas u ofensivas contra personas o instituciones.</p>
<p>XI. El diseño del logo no podrá contener alusiones, formas y colores con temas
relacionados con partidos políticos y cuestiones religiosas.</p>
<p> XII. Los ganadores del diseño del logo y del lema deberán firmar una carta de
cesión de derechos, que libera a la Universidad del Caribe de cualquier
responsabilidad ante el reclamo de terceros.</p>
</h3>

<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>4. Recepción y registro de diseños.</b>
  </h2>


  <h3 style={{textAlign:"justify", fontSize:"18px"}}> 

  La recepción de los diseños de logo y/o del lema será a partir del día de la
publicación de la presente convocatoria y hasta las 23:59 horas del día 24 de
marzo de . Los diseños de logo y lema se deberán enviar de manera
electrónica al correo: <b style={{color:"blue"}}>congresoingenieria@ucaribe.edu.mx.</b> En asunto de correo
se deberá indicar la o las categorías en la que participa y en el cuerpo del correo
el nombre completo de los integrantes en forma de lista.
Los participantes deben manifestar bajo protesta de decir verdad, en la carta de
descripción del diseño del logo y/o lema que el logo y/o lema es producto de su
creatividad y trabajo personal o en equipo, y que no se han infringido los derechos de autor o de propiedad intelectual de otras personas; asimismo, deberán
manifestar que solo han utilizado elementos de diseño realizados por el (los)
participante(s).
Los trabajos ingresados al concurso se integrarán como parte de una memoria del
concurso.
</h3>

<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>5. Integración del Jurado y evaluación de los diseños.</b>
  </h2>


  <h3 style={{textAlign:"justify" , fontSize:"18px"}}> 

  El Jurado del concurso estará integrado por los Profesores del Comité
Organizador del CEI  y el diseñador gráfico de la Universidad del Caribe.
El Jurado seleccionará al ganador de cada categoría (lema y logo), de entre las
propuestas enviadas.
<p>El Jurado tendrá la facultad de proponer ajustes en el diseño del logo y lema
ganador, sin que éstos afecten la esencia de las propuestas.</p>
<p>La decisión del jurado será inapelable e irrevocable y las propuestas y trabajos de
diseño del logo y lema serán evaluadas con apego a los requerimientos
establecidos en esta convocatoria.</p>
<p> El diseño del logo y lema ganadores formarán parte de la imagen del CEI  y
se aplicará en todos los medios impresos y digitales, así como en las diversas
comunicaciones internas y externas.</p>

</h3>


<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>6. Resultados y Premiación</b>
  </h2>


  <h3 style={{textAlign:"justify" , fontSize:"18px"}}> 

  Los resultados se darán a conocer el 14 de abril del presente año, en la página de
Internet: http://congresoingenierias.unicaribe.mx/ y http://www.ucaribe.edu.mx, de
manera directa en el correo electrónico proporcionado al momento del registro del
diseño del logo y lema.
<p>La ceremonia de premiación se llevará a cabo durante el CEI .</p>
<p> Se entregarán los siguientes premios en ambas categorías: Diploma e inscripción al CEI  y premio sorpresa.</p>
</h3>

<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>7. Propiedad de la obra.</b>
  </h2>


  <h3 style={{textAlign:"justify" , fontSize:"18px"}}> 

  El logo y lema elegido contará con el consentimiento expreso del ganador a ceder
en exclusiva a la Universidad del Caribe los derechos de uso, reproducción,
cesión, distribución, comunicación pública y transformación del diseño sin pago ni
contraprestación al ganador.

</h3>


<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>8. Aceptación de las bases.</b>
  </h2>


  <h3 style={{textAlign:"justify" , fontSize:"18px"}}> 

  La participación en esta convocatoria admite la plena aceptación de las bases. La
interpretación de las mismas o de cualquier aspecto no señalado en ellas,
corresponde única y exclusivamente al Jurado calificador del Concurso.
Las fechas establecidas en la presente convocatoria podrán estar sujetas a
cambios sin previo aviso.
</h3>

<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>9. Publicación de la convocatoria. </b>
  </h2>


  <h3 style={{textAlign:"justify" , fontSize:"18px"}}> 

  La presente convocatoria entrará en vigor el 24 de febrero de  y será
publicada en las redes sociales del DCBeI y difundido por correo electrónico
institucional.
</h3>

<h2  style={{textAlign:"left" , fontSize:"20px"}}> 
<b>10.Protección de datos personales. </b>
  </h2>


  <h3 style={{textAlign:"justify" , fontSize:"18px"}}> 
  Se informa a los participantes que los datos personales recabados serán
protegidos en los términos de las leyes aplicables. La información proporcionada
por los participantes sólo será utilizada para el procedimiento establecido en la
presente convocatoria y el registro de propiedad intelectual correspondiente.
<p>En caso de dudas o aclaraciones respecto de la presente convocatoria, se ponen
a disposición los siguientes datos de contacto:</p>
<p> Departamento de Ciencias Básicas e Ingenierías de la Universidad del Caribe, vía
correo electrónico: <b style={{color:"blue"}}>congresoingenieria@ucaribe.edu.mx.</b></p>
</h3>
            </p>


        </Container>
        </Container>
        </Container>
 
         </Layout>
    );
  }
}
