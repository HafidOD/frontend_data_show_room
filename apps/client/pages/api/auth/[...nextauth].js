import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'
import axios from 'axios'
export default NextAuth({
  // Configure one or more authentication providers
  providers: [
    Providers.Credentials({
      // The name to display on the sign in form (e.g. 'Sign in with...')
      name: 'ucaribe email',
      // The credentials is used to generate a suitable form on the sign in page.
      // You can specify whatever fields you are expecting to be submitted.
      // e.g. domain, username, password, 2FA token, etc.
      credentials: {
        email: { label: "Email", type: "text", placeholder: "mail@ucaribe.edu.mx" },
        password: {  label: "Password", type: "password" }
      },
      async authorize(credentials) {
        try{
          const {data:user} = await axios.post('http://localhost:7000/userAuth/login',credentials)
          user.name = user.firstName + " " + user.lastName;
          return user;
        } catch(ex){
          // You need to provide your own logic here that takes the credentials
          // submitted and returns either a object representing a user or value
          // that is false/null if the credentials are invalid.
          // e.g. return { id: 1, name: 'J Smith', email: 'jsmith@example.com' }
          return null
        }
      }
    }),
    // ...add more providers here
  ],
  secret: "MY_SUPER_SECRET",

})