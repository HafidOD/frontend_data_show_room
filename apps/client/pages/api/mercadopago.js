import mercadopago from 'mercadopago';

export default async function handler(req, res) {
  try{
    const { ticketId, userIdentification } = req.query

    // En esta parte obtienes los tickets y lo matcheas con el id


    // cotejas los datos del ticket con el de items
    // en external reference pones el user_identification y yap :v

    mercadopago.configure({
      access_token: 'TEST-8928262126596770-050415-d0e8cffe2ca1a8c1df68b401fa398600-442942808' // 'TEST-966a4e96-4d90-4561-8ad5-2dc01094bee3'
    });

    console.log('passed to preferences')

    let preference = {
      items: [
        {
          title: 'Mi producto', // ticket.Name
          unit_price: 200, // ticket.price
          quantity: 1,
        }
      ],
      external_reference: userIdentification
    };
    console.log('loaded preferences')

    let initPoint = '';
    const response = await mercadopago.preferences.create(preference)
    initPoint = response.body.init_point

    console.log(response)
    res.status(200).json({init_point:initPoint});
  } catch(err){
    res.status(500).json({ message: 'Server Error' })
  }
}