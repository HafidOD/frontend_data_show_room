# ucaribe.web.congresoingenierias

## Install common devDependencies

With workspaces, you can safely install devDependencies at the root of the project so that they’re listed in the package.json of the monorepo itself.

When doing so, you’ll need to confirm that this is your intention using the -W flag.